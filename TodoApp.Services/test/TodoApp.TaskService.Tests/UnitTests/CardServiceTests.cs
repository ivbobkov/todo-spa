﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

using Autofac.Extras.Moq;
using AutoMapper;
using FluentAssertions;
using Moq;

using TodoApp.Common;
using TodoApp.TaskService.BusinessLogic.Card;
using TodoApp.TaskService.DataAccess;
using TodoApp.TaskService.DataAccess.Entity;
using TodoApp.TaskService.Tests.Fakers.Card;
using TodoApp.TaskService.Tests.Fakers.Entity;
using TodoApp.Tests.Common.Fakers;
using Xunit;

namespace TodoApp.TaskService.Tests.UnitTests
{
    public class CardServiceTests
    {
        private readonly ICardService subject;
        private readonly IMapper mapper = MapperProvider.Instance;
        private readonly Mock<ITokenProvider> fakeTokenProvider;
        private readonly Mock<IGroupRepository> fakeGroupRepository;
        private readonly Mock<ICardRepository> fakeCardRepository;

        public CardServiceTests()
        {
            var mocker = AutoMock.GetLoose();

            mocker.Provide(mapper);
            fakeGroupRepository = mocker.Mock<IGroupRepository>();
            fakeCardRepository = mocker.Mock<ICardRepository>();
            fakeTokenProvider = mocker.Mock<ITokenProvider>();

            subject = mocker.Create<CardService>();
        }

        [Fact]
        public async Task FindAsync_ExistedId_CardBoard()
        {
            var entity = CardEntityFaker.Any.Generate();
            var token = AuthTokenFaker.Any.Generate();

            fakeTokenProvider.SetupGet(x => x.Token).Returns(token);
            fakeCardRepository.Setup(x => x.FindAsync(entity.Id)).ReturnsAsync(entity);
            fakeCardRepository.Setup(x => x.HasAccessAsync(entity.Id, token.UserId)).ReturnsAsync(true);

            // act
            var board = await subject.FindAsync(entity.Id);

            // assert
            board.Id.Should().Be(entity.Id);
            board.Name.Should().Be(entity.Name);
            board.Description.Should().Be(entity.Description);
            board.Complete.Should().Be(entity.Complete);
        }

        [Fact]
        public async Task UpdateAsync_ValidData_NormalFlow()
        {
            var request = UpdateCardRequestFaker.Any.Generate();
            var token = AuthTokenFaker.Any.Generate();

            fakeTokenProvider.SetupGet(x => x.Token).Returns(token);
            fakeCardRepository.Setup(x => x.UpdateAsync(It.IsAny<CardEntity>())).Returns(Task.CompletedTask);
            fakeCardRepository.Setup(x => x.HasAccessAsync(request.CardId, token.UserId)).ReturnsAsync(true);

            // act
            await subject.UpdateAsync(request);

            // assert
            Expression<Func<CardEntity, bool>> comparer = (group) =>
                group.Id == request.CardId
                && group.GroupId == request.GroupId
                && group.Complete == request.Complete
                && group.Name == request.Name;

            fakeCardRepository.Verify(x => x.UpdateAsync(It.Is(comparer)), Times.Once);
        }
    }
}
