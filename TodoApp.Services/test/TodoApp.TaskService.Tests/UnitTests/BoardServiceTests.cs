﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

using Autofac.Extras.Moq;
using AutoMapper;
using FluentAssertions;
using Moq;

using TodoApp.Common;
using TodoApp.TaskService.BusinessLogic;
using TodoApp.TaskService.BusinessLogic.Board;
using TodoApp.TaskService.DataAccess;
using TodoApp.TaskService.DataAccess.Entity;
using TodoApp.TaskService.Tests.Fakers.Board;
using TodoApp.TaskService.Tests.Fakers.Entity;
using TodoApp.Tests.Common.Fakers;
using Xunit;

namespace TodoApp.TaskService.Tests.UnitTests
{
    public class BoardServiceTests
    {
        private readonly IBoardService subject;
        private readonly IMapper mapper = MapperProvider.Instance;
        private readonly Mock<ITokenProvider> fakeTokenProvider;
        private readonly Mock<IBoardRepository> fakeBoardRepository;

        public BoardServiceTests()
        {
            var mocker = AutoMock.GetLoose();

            mocker.Provide(mapper);
            fakeBoardRepository = mocker.Mock<IBoardRepository>();
            fakeTokenProvider = mocker.Mock<ITokenProvider>();

            subject = mocker.Create<BoardService>();
        }

        [Fact]
        public async Task FindAsync_ExistedId_ReturnsBoard()
        {
            var entity = BoardEntityFaker.Any.Generate();
            var token = AuthTokenFaker.Any.Generate();

            fakeTokenProvider.SetupGet(x => x.Token).Returns(token);
            fakeBoardRepository.Setup(x => x.FindAsync(entity.Id)).ReturnsAsync(entity);
            fakeBoardRepository.Setup(x => x.HasAccessAsync(entity.Id, token.UserId)).ReturnsAsync(true);

            // act
            var board = await subject.FindAsync(entity.Id);

            // assert
            board.Id.Should().Be(entity.Id);
            board.Name.Should().Be(entity.Name);
        }

        [Fact]
        public async Task UpdateAsync_ValidData_NormalFlow()
        {
            var request = UpdateBoardRequestFaker.Any.Generate();
            var token = AuthTokenFaker.Any.Generate();
            var entity = BoardEntityFaker.Any.RuleFor(x => x.Id, request.BoardId).Generate();

            fakeTokenProvider.SetupGet(x => x.Token).Returns(token);
            fakeBoardRepository.Setup(x => x.UpdateAsync(It.IsAny<BoardEntity>())).Returns(Task.CompletedTask);
            fakeBoardRepository.Setup(x => x.HasAccessAsync(entity.Id, token.UserId)).ReturnsAsync(true);

            // act
            await subject.UpdateAsync(request);

            // assert
            Expression<Func<BoardEntity, bool>> comparer = (board) =>
                board.Id == request.BoardId
                && board.Name == request.Name;

            fakeBoardRepository.Verify(x => x.UpdateAsync(It.Is(comparer)), Times.Once);
        }

        [Fact]
        public async Task UpdateAsync_NoAccess_ThrowException()
        {
            var request = UpdateBoardRequestFaker.Any.Generate();
            var token = AuthTokenFaker.Any.Generate();

            fakeTokenProvider.SetupGet(x => x.Token).Returns(token);
            fakeBoardRepository.Setup(x => x.HasAccessAsync(request.BoardId, token.UserId)).ReturnsAsync(false);

            // act
            await Assert.ThrowsAsync<AccessDeniedException>(() => subject.UpdateAsync(request));
        }

        [Fact]
        public async Task CreateAsync_ValidData_NormalFlow()
        {
            var request = CreateBoardRequestFaker.Any.Generate();
            var token = AuthTokenFaker.Any.Generate();
            var entity = BoardEntityFaker.Any.Generate();

            fakeTokenProvider.SetupGet(x => x.Token).Returns(token);
            fakeBoardRepository.Setup(x => x.CreateAsync(It.IsAny<BoardEntity>())).ReturnsAsync(entity);

            // act
            var boardId = await subject.CreateAsync(request);

            // assert
            boardId.Should().Be(entity.Id);

            Expression<Func<BoardEntity, bool>> comparer = (board) =>
                board.Name == request.Name
                && board.UserId == token.UserId;

            fakeBoardRepository.Verify(x => x.CreateAsync(It.Is(comparer)));
        }
    }
}
