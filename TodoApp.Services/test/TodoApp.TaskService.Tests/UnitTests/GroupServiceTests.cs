﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

using Autofac.Extras.Moq;
using AutoMapper;
using FluentAssertions;
using Moq;

using TodoApp.Common;
using TodoApp.TaskService.BusinessLogic.Group;
using TodoApp.TaskService.DataAccess;
using TodoApp.TaskService.DataAccess.Entity;
using TodoApp.TaskService.Tests.Fakers.Entity;
using TodoApp.TaskService.Tests.Fakers.Group;
using TodoApp.Tests.Common.Fakers;
using Xunit;

namespace TodoApp.TaskService.Tests.UnitTests
{
    public class GroupServiceTests
    {
        private readonly IGroupService subject;
        private readonly IMapper mapper = MapperProvider.Instance;
        private readonly Mock<ITokenProvider> fakeTokenProvider;
        private readonly Mock<IBoardRepository> fakeBoardRepository;
        private readonly Mock<IGroupRepository> fakeGroupRepository;

        public GroupServiceTests()
        {
            var mocker = AutoMock.GetLoose();

            mocker.Provide(mapper);
            fakeTokenProvider = mocker.Mock<ITokenProvider>();
            fakeBoardRepository = mocker.Mock<IBoardRepository>();
            fakeGroupRepository = mocker.Mock<IGroupRepository>();

            subject = mocker.Create<GroupService>();
        }

        [Fact]
        public async Task FindAsync_ExistedId_GroupBoard()
        {
            var entity = GroupEntityFaker.Any.Generate();
            var token = AuthTokenFaker.Any.Generate();

            fakeTokenProvider.SetupGet(x => x.Token).Returns(token);
            fakeGroupRepository.Setup(x => x.FindAsync(entity.Id)).ReturnsAsync(entity);
            fakeGroupRepository.Setup(x => x.HasAccessAsync(entity.Id, token.UserId)).ReturnsAsync(true);

            // act
            var board = await subject.FindAsync(entity.Id);

            // assert
            board.Id.Should().Be(entity.Id);
            board.Name.Should().Be(entity.Name);
        }

        [Fact]
        public async Task UpdateAsync_ValidData_NormalFlow()
        {
            var request = UpdateGroupRequestFaker.Any.Generate();
            var token = AuthTokenFaker.Any.Generate();

            fakeTokenProvider.SetupGet(x => x.Token).Returns(token);
            fakeGroupRepository.Setup(x => x.HasAccessAsync(request.GroupId, token.UserId)).ReturnsAsync(true);
            fakeGroupRepository.Setup(x => x.UpdateAsync(It.IsAny<GroupEntity>())).Returns(Task.CompletedTask);

            // act
            await subject.UpdateAsync(request);

            // assert
            Expression<Func<GroupEntity, bool>> comparer = (group) =>
                group.Id == request.GroupId
                && group.BoardId == request.BoardId
                && group.Name == request.Name;

            fakeGroupRepository.Verify(x => x.UpdateAsync(It.Is(comparer)), Times.Once);
        }
    }
}
