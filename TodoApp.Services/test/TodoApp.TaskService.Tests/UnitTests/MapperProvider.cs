﻿using AutoMapper;
using TodoApp.TaskService.BusinessLogic.Board;
using TodoApp.TaskService.BusinessLogic.Card;
using TodoApp.TaskService.BusinessLogic.Group;

namespace TodoApp.TaskService.Tests.UnitTests
{
    public static class MapperProvider
    {
        public static MapperConfiguration Configuration
        {
            get
            {
                return new MapperConfiguration(c =>
                {
                    c.AddProfile<BoardServiceMapping>();
                    c.AddProfile<GroupServiceMapping>();
                    c.AddProfile<CardServiceMapping>();
                });
            }
        }

        public static IMapper Instance => Configuration.CreateMapper();
    }
}
