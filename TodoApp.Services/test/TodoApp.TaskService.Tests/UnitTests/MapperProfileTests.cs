﻿using Xunit;

namespace TodoApp.TaskService.Tests.UnitTests
{
    public class MapperProfileTests
    {
        [Fact]
        public void TestProfiles_NoExceptions()
        {
            var configuration = MapperProvider.Configuration;
            configuration.AssertConfigurationIsValid();
        }
    }
}
