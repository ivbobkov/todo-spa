﻿using System;

using Bogus;
using TodoApp.TaskService.BusinessLogic.Board;

namespace TodoApp.TaskService.Tests.Fakers.Board
{
    public static class UpdateBoardRequestFaker
    {
        public static Faker<UpdateBoardRequest> Any
        {
            get
            {
                return new Faker<UpdateBoardRequest>()
                    .RuleFor(x => x.BoardId, Guid.NewGuid)
                    .RuleFor(x => x.Name, x => x.Random.Word());
            }
        }
    }
}
