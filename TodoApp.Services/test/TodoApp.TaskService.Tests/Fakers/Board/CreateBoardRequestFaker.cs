﻿using Bogus;
using TodoApp.TaskService.BusinessLogic.Board;

namespace TodoApp.TaskService.Tests.Fakers.Board
{
    public static class CreateBoardRequestFaker
    {
        public static Faker<CreateBoardRequest> Any
        {
            get
            {
                return new Faker<CreateBoardRequest>()
                    .RuleFor(x => x.Name, x => x.Random.Word());
            }
        }
    }
}
