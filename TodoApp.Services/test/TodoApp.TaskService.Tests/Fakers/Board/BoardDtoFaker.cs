﻿using System;

using Bogus;
using TodoApp.TaskService.BusinessLogic.Board;

namespace TodoApp.TaskService.Tests.Fakers.Board
{
    public static class BoardDtoFaker
    {
        public static Faker<BoardDto> Any
        {
            get
            {
                return new Faker<BoardDto>()
                    .RuleFor(x => x.Id, Guid.NewGuid)
                    .RuleFor(x => x.Name, x => x.Lorem.Word());
            }
        }
    }
}
