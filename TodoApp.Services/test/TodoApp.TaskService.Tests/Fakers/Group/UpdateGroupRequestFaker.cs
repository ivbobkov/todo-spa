﻿using System;

using Bogus;
using TodoApp.TaskService.BusinessLogic.Group;

namespace TodoApp.TaskService.Tests.Fakers.Group
{
    public static class UpdateGroupRequestFaker
    {
        public static Faker<UpdateGroupRequest> Any
        {
            get
            {
                return new Faker<UpdateGroupRequest>()
                    .RuleFor(x => x.GroupId, Guid.NewGuid)
                    .RuleFor(x => x.BoardId, Guid.NewGuid)
                    .RuleFor(x => x.Name, x => x.Random.Word());
            }
        }
    }
}
