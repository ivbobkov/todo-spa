﻿using System;

using Bogus;
using TodoApp.TaskService.BusinessLogic.Group;

namespace TodoApp.TaskService.Tests.Fakers.Group
{
    public static class GroupDtoFaker
    {
        public static Faker<GroupDto> Any
        {
            get
            {
                return new Faker<GroupDto>()
                    .RuleFor(x => x.Id, Guid.NewGuid)
                    .RuleFor(x => x.Name, x => x.Lorem.Word())
                    .RuleFor(x => x.BoardId, Guid.NewGuid);
            }
        }
    }
}
