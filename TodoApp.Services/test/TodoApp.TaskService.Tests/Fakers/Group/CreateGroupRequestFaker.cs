﻿using System;

using Bogus;
using TodoApp.TaskService.BusinessLogic.Card;

namespace TodoApp.TaskService.Tests.Fakers.Group
{
    public static class CreateGroupRequestFaker
    {
        public static Faker<CreateCardRequest> Any
        {
            get
            {
                return new Faker<CreateCardRequest>()
                    .RuleFor(x => x.GroupId, Guid.NewGuid)
                    .RuleFor(x => x.Name, x => x.Random.Word());
            }
        }
    }
}
