﻿using System;

using Bogus;
using TodoApp.TaskService.DataAccess.Entity;

namespace TodoApp.TaskService.Tests.Fakers.Entity
{
    public static class BoardEntityFaker
    {
        public static Faker<BoardEntity> Any
        {
            get
            {
                return new Faker<BoardEntity>()
                    .RuleFor(x => x.Id, Guid.NewGuid)
                    .RuleFor(x => x.Name, x => x.Random.Word())
                    .RuleFor(x => x.UserId, Guid.NewGuid);
            }
        }
    }
}
