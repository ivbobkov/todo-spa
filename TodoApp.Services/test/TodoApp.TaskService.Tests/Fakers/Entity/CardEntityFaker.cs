﻿using System;

using Bogus;
using TodoApp.TaskService.DataAccess.Entity;

namespace TodoApp.TaskService.Tests.Fakers.Entity
{
    public static class CardEntityFaker
    {
        public static Faker<CardEntity> Any
        {
            get
            {
                return new Faker<CardEntity>()
                    .RuleFor(x => x.Id, Guid.NewGuid)
                    .RuleFor(x => x.Name, x => x.Random.Word())
                    .RuleFor(x => x.Description, x => x.Random.Word())
                    .RuleFor(x => x.Complete, x => x.Random.Bool())
                    .RuleFor(x => x.Group, x => GroupEntityFaker.Any)
                    .RuleFor(x => x.GroupId, (f, x) => x.GroupId = x.Group.Id)
                    .FinishWith((f, x) => x.Group.Cards.Add(x));
            }
        }
    }
}
