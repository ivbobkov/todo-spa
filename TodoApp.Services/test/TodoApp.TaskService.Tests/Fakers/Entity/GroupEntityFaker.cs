﻿using System;

using Bogus;
using TodoApp.TaskService.DataAccess.Entity;

namespace TodoApp.TaskService.Tests.Fakers.Entity
{
    public static class GroupEntityFaker
    {
        public static Faker<GroupEntity> Any
        {
            get
            {
                return new Faker<GroupEntity>()
                    .RuleFor(x => x.Id, Guid.NewGuid)
                    .RuleFor(x => x.Name, x => x.Random.Word())
                    .RuleFor(x => x.Board, x => BoardEntityFaker.Any)
                    .RuleFor(x => x.BoardId, (f, x) => x.BoardId = x.Board.Id)
                    .FinishWith((f, x) => x.Board.Groups.Add(x));
            }
        }
    }
}
