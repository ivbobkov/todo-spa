﻿using System;

using Bogus;
using TodoApp.TaskService.BusinessLogic.Card;

namespace TodoApp.TaskService.Tests.Fakers.Card
{
    public static class CardDtoFaker
    {
        public static Faker<CardDto> Any
        {
            get
            {
                return new Faker<CardDto>()
                    .RuleFor(x => x.Id, Guid.NewGuid)
                    .RuleFor(x => x.Name, x => x.Lorem.Word())
                    .RuleFor(x => x.Description, x => x.Random.Word())
                    .RuleFor(x => x.Complete, x => x.Random.Bool())
                    .RuleFor(x => x.GroupId, Guid.NewGuid);
            }
        }
    }
}
