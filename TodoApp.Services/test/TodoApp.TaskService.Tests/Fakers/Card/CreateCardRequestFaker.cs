﻿using System;

using Bogus;
using TodoApp.TaskService.BusinessLogic.Card;

namespace TodoApp.TaskService.Tests.Fakers.Card
{
    public class CreateCardRequestFaker
    {
        public static Faker<CreateCardRequest> Any
        {
            get
            {
                return new Faker<CreateCardRequest>()
                    .RuleFor(x => x.GroupId, Guid.NewGuid)
                    .RuleFor(x => x.Name, x => x.Random.Word())
                    .RuleFor(x => x.Description, x => x.Random.Word())
                    .RuleFor(x => x.Complete, x => x.Random.Bool());
            }
        }
    }
}
