﻿using System;

using Bogus;
using TodoApp.TaskService.BusinessLogic.Card;

namespace TodoApp.TaskService.Tests.Fakers.Card
{
    public static class UpdateCardRequestFaker
    {
        public static Faker<UpdateCardRequest> Any
        {
            get
            {
                return new Faker<UpdateCardRequest>()
                    .RuleFor(x => x.CardId, Guid.NewGuid)
                    .RuleFor(x => x.Name, x => x.Random.Word())
                    .RuleFor(x => x.Description, x => x.Random.Word())
                    .RuleFor(x => x.Complete, x => x.Random.Bool());
            }
        }
    }
}
