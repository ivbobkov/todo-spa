﻿using System;
using Bogus;
using TodoApp.Common;

namespace TodoApp.Tests.Common.Fakers
{
    public static class AuthTokenFaker
    {
        public static Faker<SecurityToken> Any
        {
            get
            {
                return new Faker<SecurityToken>()
                    .RuleFor(x => x.UserId, Guid.NewGuid)
                    .RuleFor(x => x.Username, x => x.Internet.UserName());
            }
        }
    }
}
