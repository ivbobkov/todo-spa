﻿using System;
using Bogus;
using TodoApp.AccountService.DataAccess.Entity;

namespace TodoApp.AccountService.Tests.Fakers
{
    public static class UserEntityFaker
    {
        public static Faker<UserEntity> Any
        {
            get
            {
                return new Faker<UserEntity>()
                    .RuleFor(x => x.Id, Guid.NewGuid)
                    .RuleFor(x => x.Username, x => x.Random.Word())
                    .RuleFor(x => x.PasswordHash, x => Guid.NewGuid().ToString());
            }
        }
    }
}
