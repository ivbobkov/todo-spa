﻿using Bogus;
using TodoApp.AccountService.BusinessLogic.Token;

namespace TodoApp.AccountService.Tests.Fakers.Token
{
    public static class AuthTokenRequestFaker
    {
        public static Faker<TokenRequest> Any
        {
            get
            {
                return new Faker<TokenRequest>()
                    .RuleFor(x => x.Username, x => x.Internet.UserName())
                    .RuleFor(x => x.Password, x => x.Internet.Password());
            }
        }
    }
}
