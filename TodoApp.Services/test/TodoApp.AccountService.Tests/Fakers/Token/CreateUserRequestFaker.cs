﻿using Bogus;
using TodoApp.AccountService.BusinessLogic.User;
using TodoApp.AccountService.DataAccess.Entity;

namespace TodoApp.AccountService.Tests.Fakers.Token
{
    public static class CreateUserRequestFaker
    {
        public static Faker<CreateUserRequest> Any
        {
            get
            {
                return new Faker<CreateUserRequest>()
                    .RuleFor(x => x.Username, x => x.Internet.UserName())
                    .RuleFor(x => x.Email, x => x.Internet.Email())
                    .RuleFor(x => x.Password, x => x.Internet.Password());
            }
        }

        public static UserEntity FromCreateRequest(CreateUserRequest request)
        {
            return new UserEntity
            {
                Email = request.Email,
                Username = request.Username
            };
        }
    }
}