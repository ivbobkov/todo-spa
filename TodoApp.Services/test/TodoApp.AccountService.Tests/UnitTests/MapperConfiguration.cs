﻿using AutoMapper;
using TodoApp.AccountService.BusinessLogic.Token;
using TodoApp.AccountService.BusinessLogic.User;

namespace TodoApp.AccountService.Tests.UnitTests
{
    public static class MapperConfiguration
    {
        public static AutoMapper.MapperConfiguration Configuration
        {
            get
            {
                return new AutoMapper.MapperConfiguration(c =>
                {
                    c.AddProfile<UserServiceMapping>();
                    c.AddProfile<TokenServiceMapping>();
                });
            }
        }

        public static IMapper Instance => Configuration.CreateMapper();
    }
}
