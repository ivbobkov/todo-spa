﻿using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Autofac.Extras.Moq;
using AutoMapper;
using FluentAssertions;
using Moq;
using TodoApp.AccountService.BusinessLogic.User;
using TodoApp.AccountService.DataAccess;
using TodoApp.AccountService.DataAccess.Entity;
using TodoApp.AccountService.Tests.Fakers.Token;
using Xunit;

namespace TodoApp.AccountService.Tests.UnitTests
{
    public class UserServiceTests
    {
        private readonly IUserService subject;
        private readonly IMapper mapper;

        private readonly Mock<IUserRepository> fakeUserRepository;

        public UserServiceTests()
        {
            var mocker = AutoMock.GetLoose();

            fakeUserRepository = mocker.Mock<IUserRepository>();

            mapper = MapperConfiguration.Instance;
            mocker.Provide(mapper);

            subject = mocker.Create<UserService>();
        }

        [Fact]
        public async Task CreateAsync_ValidData_NormalFlow()
        {
            var request = CreateUserRequestFaker.Any.Generate();

            var userId = Guid.NewGuid();
            var userEntity = CreateUserRequestFaker.FromCreateRequest(request);
            fakeUserRepository
                .Setup(x => x.CreateAsync(It.Is<UserEntity>(c => c.Username == request.Username && c.Email == request.Email)))
                .ReturnsAsync(() =>
                {
                    userEntity.Id = userId;
                    return userEntity;
                });

            var result = await subject.CreateAsync(request);

            result.Should().Be(userId);
        }
    }
}