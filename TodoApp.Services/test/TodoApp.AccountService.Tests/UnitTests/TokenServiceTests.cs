﻿using System;
using System.Threading.Tasks;
using Autofac.Extras.Moq;
using FluentAssertions;
using Moq;
using TodoApp.AccountService.BusinessLogic.Common;
using TodoApp.AccountService.BusinessLogic.Token;
using TodoApp.AccountService.DataAccess;
using TodoApp.AccountService.Tests.Fakers;
using TodoApp.AccountService.Tests.Fakers.Token;
using TodoApp.Common;
using Xunit;

namespace TodoApp.AccountService.Tests.UnitTests
{
    public class TokenServiceTests
    {
        private readonly ITokenService subject;

        private readonly Mock<ITokenHandler> fakeTokenHandler;
        private readonly Mock<IPasswordHasher> fakePasswordHasher;
        private readonly Mock<IUserRepository> fakeUserRepository;

        public TokenServiceTests()
        {
            var mocker = AutoMock.GetLoose();

            fakeTokenHandler = mocker.Mock<ITokenHandler>();
            fakePasswordHasher = mocker.Mock<IPasswordHasher>();
            fakeUserRepository = mocker.Mock<IUserRepository>();

            mocker.Provide(MapperConfiguration.Instance);

            subject = mocker.Create<TokenService>();
        }

        [Fact]
        public async Task ProvideTokenAsync_NormalFlow_ProvideToken()
        {
            const string token = "the_token";
            var request = AuthTokenRequestFaker.Any.Generate();
            var user = UserEntityFaker.Any.Generate();

            fakeUserRepository.Setup(x => x.FindAsync(request.Username)).ReturnsAsync(user);
            fakeTokenHandler.Setup(x => x.CreateToken(It.IsAny<SecurityToken>())).Returns(token);
            fakePasswordHasher.Setup(x => x.HashPassword(request.Password)).Returns(user.PasswordHash);

            // act
            var response = await subject.ProvideTokenAsync(request);

            // assert
            response.Should().BeEquivalentTo(token);
            fakeTokenHandler.Verify(x => x.CreateToken(It.Is<SecurityToken>(c => c.UserId == user.Id && c.Username == user.Username)));
        }

        [Fact]
        public async Task ProvideTokenAsync_NoUser_ThrowsException()
        {
            var request = AuthTokenRequestFaker.Any.Generate();

            fakeUserRepository.Setup(x => x.FindAsync(request.Username)).ReturnsAsync(() => null);

            // act
            await Assert.ThrowsAsync<Exception>(() => subject.ProvideTokenAsync(request));
        }

        [Fact]
        public async Task ProvideTokenAsync_InvalidPassword_ThrowsException()
        {
            var request = AuthTokenRequestFaker.Any.Generate();
            var user = UserEntityFaker.Any.Generate();

            fakeUserRepository.Setup(x => x.FindAsync(request.Username)).ReturnsAsync(() => user);
            fakeTokenHandler.Setup(x => x.CreateToken(It.IsAny<SecurityToken>())).Returns("Token-1");
            fakePasswordHasher.Setup(x => x.HashPassword(request.Password)).Returns("Hash-1");

            // act
            await Assert.ThrowsAsync<Exception>(() => subject.ProvideTokenAsync(request));
        }
    }
}
