﻿using Nancy;
using Nancy.ModelBinding;

using TodoApp.AccountService.BusinessLogic.Token;
using TodoApp.Common.Web;
using TodoApp.Common.Web.Nancy;

namespace TodoApp.AccountService.Modules
{
    public class TokenModule : NancyModule
    {
        private readonly ITokenService tokenService;

        public TokenModule(ITokenService tokenService) : base("/api/tokens")
        {
            this.tokenService = tokenService;
            DefineRoutes();
        }

        private void DefineRoutes()
        {
            Post("/",
                async parameters =>
                {
                    var request = this.Bind<TokenRequest>();
                    var tokenResponse = await tokenService.ProvideTokenAsync(request);
                    return ApiResponse.Success(tokenResponse);
                });

            OnError.AddErrorHandler();
        }
    }
}
