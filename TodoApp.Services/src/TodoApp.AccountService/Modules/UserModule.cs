﻿using System;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Security;

using TodoApp.AccountService.BusinessLogic.User;
using TodoApp.Common.Web;
using TodoApp.Common.Web.Nancy;

namespace TodoApp.AccountService.Modules
{
    public class UserModule : NancyModule
    {
        private readonly IUserService userService;

        public UserModule(IUserService userService) : base("/api/users")
        {
            this.userService = userService;
            DefineRoutes();
        }

        private void DefineRoutes()
        {
            Get("/{userId:guid}",
                async parameters =>
                {
                    this.RequiresAuthentication();
                    var userId = (Guid)parameters.userId;
                    var userDto = await userService.FindAsync(userId);
                    return ApiResponse.Success(userDto);
                });

            Post("/",
                async parameters =>
                {
                    var request = this.Bind<CreateUserRequest>();
                    var userId = await userService.CreateAsync(request);
                    return ApiResponse.Success(userId);
                });

            OnError.AddErrorHandler();
        }
    }
}
