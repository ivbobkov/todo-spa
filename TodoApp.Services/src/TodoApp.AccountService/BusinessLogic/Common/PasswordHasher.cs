﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace TodoApp.AccountService.BusinessLogic.Common
{
    public class PasswordHasher : IPasswordHasher
    {
        /// <inheritdoc />
        public string HashPassword(string password)
        {
            var algorithm = MD5.Create();
            var hash = algorithm.ComputeHash(Encoding.UTF8.GetBytes(password));
            return Convert.ToBase64String(hash);
        }
    }
}
