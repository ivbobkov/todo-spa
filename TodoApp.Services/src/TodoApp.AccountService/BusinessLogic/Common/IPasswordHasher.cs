﻿namespace TodoApp.AccountService.BusinessLogic.Common
{
    public interface IPasswordHasher
    {
        string HashPassword(string password);
    }
}
