﻿using System;
using System.Threading.Tasks;

namespace TodoApp.AccountService.BusinessLogic.User
{
    public interface IUserService
    {
        Task<UserDto> FindAsync(Guid userId);
        Task<Guid> CreateAsync(CreateUserRequest request);
    }
}
