﻿using AutoMapper;

using TodoApp.AccountService.DataAccess.Entity;

namespace TodoApp.AccountService.BusinessLogic.User
{
    public class UserServiceMapping : Profile
    {
        public UserServiceMapping()
        {
            CreateMap<CreateUserRequest, UserEntity>()
                .ForMember(x => x.Id, x => x.Ignore())
                .ForMember(x => x.Username, x => x.Ignore())
                .ForMember(x => x.PasswordHash, x => x.Ignore());
        }
    }
}
