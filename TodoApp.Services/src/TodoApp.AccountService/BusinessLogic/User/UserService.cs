﻿using System;
using System.Threading.Tasks;

using AutoMapper;

using TodoApp.AccountService.BusinessLogic.Common;
using TodoApp.AccountService.DataAccess;
using TodoApp.AccountService.DataAccess.Entity;

namespace TodoApp.AccountService.BusinessLogic.User
{
    public class UserService : IUserService
    {
        private readonly IMapper mapper;
        private readonly IPasswordHasher passwordHasher;
        private readonly IUserRepository userRepository;

        public UserService(
            IMapper mapper,
            IPasswordHasher passwordHasher,
            IUserRepository userRepository)
        {
            this.mapper = mapper;
            this.passwordHasher = passwordHasher;
            this.userRepository = userRepository;
        }

        /// <inheritdoc />
        public async Task<UserDto> FindAsync(Guid userId)
        {
            var entity = await userRepository.FindAsync(userId);
            return mapper.Map<UserDto>(entity);
        }

        /// <inheritdoc />
        public async Task<Guid> CreateAsync(CreateUserRequest request)
        {
            var userEntity = new UserEntity
            {
                Username = request.Username,
                Email = request.Email,
                PasswordHash = passwordHasher.HashPassword(request.Password)
            };

            userEntity = await userRepository.CreateAsync(userEntity);

            return userEntity.Id;
        }
    }
}
