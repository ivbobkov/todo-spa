﻿using System;

namespace TodoApp.AccountService.BusinessLogic.User
{
    public class UserDto
    {
        public Guid Id { get; set; }

        public string Username { get; set; }
    }
}
