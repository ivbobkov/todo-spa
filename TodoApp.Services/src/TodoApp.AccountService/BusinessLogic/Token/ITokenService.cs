﻿using System.Threading.Tasks;

namespace TodoApp.AccountService.BusinessLogic.Token
{
    public interface ITokenService
    {
        Task<string> ProvideTokenAsync(TokenRequest request);
    }
}
