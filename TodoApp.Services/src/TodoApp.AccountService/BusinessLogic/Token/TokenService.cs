﻿using System;
using System.Threading.Tasks;

using AutoMapper;

using TodoApp.AccountService.BusinessLogic.Common;
using TodoApp.AccountService.DataAccess;
using TodoApp.Common;

namespace TodoApp.AccountService.BusinessLogic.Token
{
    public class TokenService : ITokenService
    {
        private readonly IMapper mapper;
        private readonly ITokenHandler tokenHandler;
        private readonly IPasswordHasher passwordHasher;
        private readonly IUserRepository userRepository;

        public TokenService(
            IMapper mapper,
            ITokenHandler tokenHandler,
            IPasswordHasher passwordHasher,
            IUserRepository userRepository)
        {
            this.mapper = mapper;
            this.tokenHandler = tokenHandler;
            this.passwordHasher = passwordHasher;
            this.userRepository = userRepository;
        }

        /// <inheritdoc />
        public async Task<string> ProvideTokenAsync(TokenRequest request)
        {
            var user = await userRepository.FindAsync(request.Username);

            if (user == null)
            {
                throw new Exception("User not found.");
            }

            var passwordHash = passwordHasher.HashPassword(request.Password);

            if (!passwordHash.Equals(user.PasswordHash))
            {
                throw new Exception("Invalid password");
            }

            var token = mapper.Map<SecurityToken>(user);

            return tokenHandler.CreateToken(token);
        }
    }
}
