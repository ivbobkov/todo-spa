﻿namespace TodoApp.AccountService.BusinessLogic.Token
{
    public class TokenRequest
    {
        public string Username { get; set; }

        public string Password { get; set; }
    }
}
