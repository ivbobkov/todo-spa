﻿using AutoMapper;

using TodoApp.AccountService.DataAccess.Entity;
using TodoApp.Common;

namespace TodoApp.AccountService.BusinessLogic.Token
{
    public class TokenServiceMapping : Profile
    {
        public TokenServiceMapping()
        {
            CreateMap<UserEntity, SecurityToken>()
                .ForMember(x => x.UserId, x => x.MapFrom(c => c.Id))
                .ForMember(x => x.Username, x => x.MapFrom(c => c.Username));
        }
    }
}
