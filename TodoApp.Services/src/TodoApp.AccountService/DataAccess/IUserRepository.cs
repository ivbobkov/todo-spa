﻿using System;
using System.Threading.Tasks;

using TodoApp.AccountService.DataAccess.Entity;

namespace TodoApp.AccountService.DataAccess
{
    /// <summary>
    /// The user repository
    /// </summary>
    public interface IUserRepository
    {
        /// <summary>
        /// Finds the <see cref="UserEntity"/>
        /// </summary>
        /// <param name="userId">The identifier</param>
        /// <returns>The <see cref="UserEntity"/></returns>
        Task<UserEntity> FindAsync(Guid userId);

        /// <summary>
        /// Finds the <see cref="UserEntity"/>
        /// </summary>
        /// <param name="username">The username</param>
        /// <returns>The <see cref="UserEntity"/></returns>
        Task<UserEntity> FindAsync(string username);

        /// <summary>
        /// Creates the <see cref="UserEntity"/>
        /// </summary>
        /// <param name="entity">The <see cref="UserEntity"/></param>
        /// <returns>The <see cref="UserEntity"/></returns>
        Task<UserEntity> CreateAsync(UserEntity entity);
    }
}
