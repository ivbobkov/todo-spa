﻿using System;

using Microsoft.Extensions.Logging;

namespace TodoApp.AccountService.DataAccess.Implementation
{
    public class BaseRepository : IDisposable
    {
        protected readonly ILoggerFactory LoggerFactory;
        protected readonly AccountServiceContext Context;

        public BaseRepository(ILoggerFactory loggerFactory, AccountServiceContext context)
        {
            LoggerFactory = loggerFactory;
            Context = context;
        }

        /// <inheritdoc />
        public void Dispose()
        {
            Context.Dispose();
        }
    }
}
