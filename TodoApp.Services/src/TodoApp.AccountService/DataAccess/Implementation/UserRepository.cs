﻿using System;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

using TodoApp.AccountService.DataAccess.Entity;

namespace TodoApp.AccountService.DataAccess.Implementation
{
    /// <inheritdoc cref="IUserRepository" />
    public class UserRepository : BaseRepository, IUserRepository
    {
        /// <inheritdoc />
        public UserRepository(ILoggerFactory loggerFactory, AccountServiceContext context) : base(loggerFactory, context)
        {
        }

        /// <inheritdoc />
        public async Task<UserEntity> FindAsync(Guid userId)
        {
            return await Context.Users.FirstOrDefaultAsync(x => x.Id == userId);
        }

        /// <inheritdoc />
        public async Task<UserEntity> FindAsync(string username)
        {
            var normalizedUsername = username.ToUpperInvariant();
            return await Context.Users.FirstOrDefaultAsync(x => x.Username == normalizedUsername);
        }

        /// <inheritdoc />
        public async Task<UserEntity> CreateAsync(UserEntity entity)
        {
            Context.Users.Add(entity);
            await Context.SaveChangesAsync();
            return entity;
        }
    }
}
