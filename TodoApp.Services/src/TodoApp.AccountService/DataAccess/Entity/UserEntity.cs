﻿using System;

namespace TodoApp.AccountService.DataAccess.Entity
{
    public class UserEntity
    {
        public Guid Id { get; set; }

        public string Username { get; set; }

        public string Email { get; set; }

        public string PasswordHash { get; set; }
    }
}
