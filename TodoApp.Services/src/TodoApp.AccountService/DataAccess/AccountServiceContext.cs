﻿using Microsoft.EntityFrameworkCore;

using TodoApp.AccountService.DataAccess.Entity;

namespace TodoApp.AccountService.DataAccess
{
    public class AccountServiceContext : DbContext
    {
        public AccountServiceContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Configure();
        }

        public virtual DbSet<UserEntity> Users { get; set; }
    }
}
