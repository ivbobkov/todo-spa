﻿using Microsoft.EntityFrameworkCore;

using TodoApp.AccountService.DataAccess.Entity;

namespace TodoApp.AccountService.DataAccess
{
    public static class AccountServiceContextConfiguration
    {
        public static void Configure(this ModelBuilder builder)
        {
            IdentityUser(builder);
        }

        private static void IdentityUser(ModelBuilder builder)
        {
            builder.Entity<UserEntity>(
                config =>
                {
                    config.ToTable("User");
                    config.HasIndex(x => x.Username).IsUnique();
                });
        }
    }
}
