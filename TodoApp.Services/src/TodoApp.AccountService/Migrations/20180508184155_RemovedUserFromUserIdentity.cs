﻿using System;

using Microsoft.EntityFrameworkCore.Migrations;

namespace TodoApp.AccountService.Migrations
{
    public partial class RemovedUserFromUserIdentity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_IdentityUser_UserId",
                table: "IdentityUser");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "IdentityUser");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "UserId",
                table: "IdentityUser",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_IdentityUser_UserId",
                table: "IdentityUser",
                column: "UserId",
                unique: true);
        }
    }
}
