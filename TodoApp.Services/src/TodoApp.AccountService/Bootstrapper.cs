﻿using AutoMapper;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using Nancy;
using Nancy.Authentication.Stateless;
using Nancy.Bootstrapper;
using Nancy.TinyIoc;

using TodoApp.AccountService.BusinessLogic.Common;
using TodoApp.AccountService.BusinessLogic.Token;
using TodoApp.AccountService.BusinessLogic.User;
using TodoApp.AccountService.DataAccess;
using TodoApp.AccountService.DataAccess.Implementation;
using TodoApp.Common;
using TodoApp.Common.Web.Nancy;

namespace TodoApp.AccountService
{
    public class Bootstrapper : DefaultNancyBootstrapper
    {
        private readonly IConfigurationRoot configuration;

        public Bootstrapper(IConfigurationRoot configuration)
        {
            this.configuration = configuration;
        }

        /// <inheritdoc />
        protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
        {
            container.Register<ILoggerFactory, LoggerFactory>();
            container.Register<ITokenHandler, TokenHandler>();
            container.Register(new TokenHandlerSettings { SecretKey = "SUPER-SECRET-KEY" });
            container.Register<INancyIdentityProvider, NancyIdentityProvider>();

            var identityProvider = container.Resolve<INancyIdentityProvider>();
            var statelessAuthentification = new StatelessAuthenticationConfiguration(identityProvider.GetUserIdentity);

            StatelessAuthentication.Enable(pipelines, statelessAuthentification);

            base.ApplicationStartup(container, pipelines);
        }

        /// <inheritdoc />
        protected override void ConfigureRequestContainer(TinyIoCContainer container, NancyContext context)
        {
            container.Register<IUserService, UserService>();
            container.Register<IUserRepository, UserRepository>();

            container.Register<ITokenService, TokenService>();
            container.Register<IPasswordHasher, PasswordHasher>();

            container.Register(Mapper);
            container.Register(DbContext);
        }

        private AccountServiceContext DbContext
        {
            get
            {
                var builder = new DbContextOptionsBuilder<AccountServiceContext>();
                builder.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));
                return new AccountServiceContext(builder.Options);
            }
        }

        private static IMapper Mapper
        {
            get
            {
                var configuration = new MapperConfiguration(c =>
                {
                    c.AddProfile<UserServiceMapping>();
                    c.AddProfile<TokenServiceMapping>();
                });

                return configuration.CreateMapper();
            }
        }
    }
}

