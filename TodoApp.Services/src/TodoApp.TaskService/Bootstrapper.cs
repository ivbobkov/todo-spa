﻿using AutoMapper;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using Nancy;
using Nancy.Authentication.Stateless;
using Nancy.Bootstrapper;
using Nancy.TinyIoc;

using TodoApp.Common;
using TodoApp.Common.Web.Nancy;
using TodoApp.TaskService.BusinessLogic.Board;
using TodoApp.TaskService.BusinessLogic.Card;
using TodoApp.TaskService.BusinessLogic.Group;
using TodoApp.TaskService.DataAccess;
using TodoApp.TaskService.DataAccess.Implementation;

namespace TodoApp.TaskService
{
    public class Bootstrapper : DefaultNancyBootstrapper
    {
        private readonly IConfigurationRoot configuration;

        public Bootstrapper(IConfigurationRoot configuration)
        {
            this.configuration = configuration;
        }

        /// <inheritdoc />
        protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
        {
            container.Register<ILoggerFactory, LoggerFactory>();
            container.Register<ITokenHandler, TokenHandler>();
            container.Register(new TokenHandlerSettings { SecretKey = "SUPER-SECRET-KEY" });
            container.Register<INancyIdentityProvider, NancyIdentityProvider>();

            var identityProvider = container.Resolve<INancyIdentityProvider>();
            var statelessAuthentification = new StatelessAuthenticationConfiguration(identityProvider.GetUserIdentity);

            StatelessAuthentication.Enable(pipelines, statelessAuthentification);

            base.ApplicationStartup(container, pipelines);
        }

        /// <inheritdoc />
        protected override void ConfigureRequestContainer(TinyIoCContainer container, NancyContext context)
        {
            container.Register<IBoardService, BoardService>();
            container.Register<IGroupService, GroupService>();
            container.Register<ICardService, CardService>();

            container.Register<IBoardRepository, BoardRepository>();
            container.Register<IGroupRepository, GroupRepository>();
            container.Register<ICardRepository, CardRepository>();

            container.Register<ITokenProvider>(new TokenProvider(context));

            container.Register(Mapper);
            container.Register(DbContext);
        }

        private TaskServiceContext DbContext
        {
            get
            {
                var builder = new DbContextOptionsBuilder<TaskServiceContext>();
                builder.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));
                return new TaskServiceContext(builder.Options);
            }
        }

        private static IMapper Mapper
        {
            get
            {
                var configuration = new MapperConfiguration(c =>
                {
                    c.AddProfile<BoardServiceMapping>();
                    c.AddProfile<GroupServiceMapping>();
                    c.AddProfile<CardServiceMapping>();
                });

                return configuration.CreateMapper();
            }
        }
    }
}