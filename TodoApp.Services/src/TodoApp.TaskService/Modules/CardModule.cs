﻿using Nancy;
using Nancy.ModelBinding;
using Nancy.Security;
using TodoApp.Common.Web;
using TodoApp.Common.Web.Nancy;
using TodoApp.TaskService.BusinessLogic.Card;

namespace TodoApp.TaskService.Modules
{
    public class CardModule : NancyModule
    {
        private readonly ICardService cardService;

        public CardModule(ICardService cardService) : base("/api/cards")
        {
            this.RequiresAuthentication();
            this.cardService = cardService;
            DefineRoutes();
        }

        private void DefineRoutes()
        {
            Get("/{cardId:guid}",
                async parameters =>
                {
                    var groupDto = await cardService.FindAsync(parameters.cardId);
                    return ApiResponse.Success(groupDto);
                });

            Get("/",
                async parameters =>
                {
                    var cards = await cardService.SelectByUserAsync();
                    return ApiResponse.Success(cards);
                });

            Post("/",
                async parameters =>
                {
                    var request = this.Bind<CreateCardRequest>();
                    var cardId = await cardService.CreateAsync(request);
                    return ApiResponse.Success(cardId);
                });

            Put("/{cardId:guid}",
                async parameters =>
                {
                    var request = this.Bind<UpdateCardRequest>();
                    await cardService.UpdateAsync(request);
                    return ApiResponse.Success();
                });

            Delete("/{cardId:guid}",
                async parameters =>
                {
                    await cardService.DeleteAsync(parameters.cardId);
                    return ApiResponse.Success();
                });

            OnError.AddErrorHandler();
        }
    }
}