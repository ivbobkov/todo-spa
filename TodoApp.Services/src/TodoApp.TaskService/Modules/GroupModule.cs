﻿using Nancy;
using Nancy.ModelBinding;
using Nancy.Security;
using TodoApp.Common.Web;
using TodoApp.Common.Web.Nancy;
using TodoApp.TaskService.BusinessLogic.Group;

namespace TodoApp.TaskService.Modules
{
    public class GroupModule : NancyModule
    {
        private readonly IGroupService groupService;

        public GroupModule(IGroupService groupService) : base("/api/groups")
        {
            this.RequiresAuthentication();
            this.groupService = groupService;
            DefineRoutes();
        }

        private void DefineRoutes()
        {
            Get("/{groupId:guid}",
                async parameters =>
                {
                    var groupDto = await groupService.FindAsync(parameters.groupId);
                    return ApiResponse.Success(groupDto);
                });

            Get("/",
                async parameters =>
                {
                    var groups = await groupService.SelectByUserAsync();
                    return ApiResponse.Success(groups);
                });

            Post("/",
                async parameters =>
                {
                    var request = this.Bind<CreateGroupRequest>();
                    var groupId = await groupService.CreateAsync(request);
                    return ApiResponse.Success(groupId);
                });

            Put("/{groupId:guid}",
                async parameters =>
                {
                    var request = this.Bind<UpdateGroupRequest>();
                    await groupService.UpdateAsync(request);
                    return ApiResponse.Success();
                });

            Delete("/{groupId:guid}",
                async parameters =>
                {
                    await groupService.DeleteAsync(parameters.groupId);
                    return ApiResponse.Success();
                });

            OnError.AddErrorHandler();
        }
    }
}