﻿using Nancy;
using Nancy.ModelBinding;
using Nancy.Security;
using TodoApp.Common.Web;
using TodoApp.Common.Web.Nancy;
using TodoApp.TaskService.BusinessLogic.Board;

namespace TodoApp.TaskService.Modules
{
    public class BoardModule : NancyModule
    {
        private readonly IBoardService boardService;

        public BoardModule(IBoardService boardService) : base("/api/boards")
        {
            this.RequiresAuthentication();
            this.boardService = boardService;
            DefineRoutes();
        }

        private void DefineRoutes()
        {
            Get("/{boardId:guid}",
                async parameters =>
                {
                    var boardDto = await boardService.FindAsync(parameters.boardId);
                    return ApiResponse.Success(boardDto);
                });

            Get("/",
                async parameters =>
                {
                    var boards = await boardService.SelectByUserAsync();
                    return ApiResponse.Success(boards);
                });

            Post("/",
                async parameters =>
                {
                    var request = this.Bind<CreateBoardRequest>();
                    var boardId = await boardService.CreateAsync(request);
                    return ApiResponse.Success(boardId);
                });

            Put("/{boardId:guid}",
                async parameters =>
                {
                    var request = this.Bind<UpdateBoardRequest>();
                    await boardService.UpdateAsync(request);
                    return ApiResponse.Success();
                });

            Delete("/{boardId:guid}",
                async parameters =>
                {
                    await boardService.DeleteAsync(parameters.boardId);
                    return ApiResponse.Success();
                });

            OnError.AddErrorHandler();
        }
    }
}
