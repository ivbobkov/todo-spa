﻿using System;

namespace TodoApp.TaskService.BusinessLogic.Group
{
    public class UpdateGroupRequest
    {
        public Guid GroupId { get; set; }

        public string Name { get; set; }

        public Guid BoardId { get; set; }
    }
}
