﻿using AutoMapper;

using TodoApp.TaskService.DataAccess.Entity;

namespace TodoApp.TaskService.BusinessLogic.Group
{
    public class GroupServiceMapping : Profile
    {
        public GroupServiceMapping()
        {
            CreateMap<CreateGroupRequest, GroupEntity>()
                .ForMember(x => x.Id, x => x.Ignore())
                .ForMember(x => x.Board, x => x.Ignore())
                .ForMember(x => x.Cards, x => x.Ignore());

            CreateMap<UpdateGroupRequest, GroupEntity>()
                .ForMember(x => x.Id, x => x.MapFrom(c => c.GroupId))
                .ForMember(x => x.Board, x => x.Ignore())
                .ForMember(x => x.Cards, x => x.Ignore());

            CreateMap<GroupEntity, GroupDto>()
                .ReverseMap();
        }
    }
}
