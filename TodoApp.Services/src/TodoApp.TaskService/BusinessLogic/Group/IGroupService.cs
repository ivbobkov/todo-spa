﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TodoApp.TaskService.BusinessLogic.Group
{
    public interface IGroupService
    {
        Task<GroupDto> FindAsync(Guid groupId);

        Task<List<GroupDto>> SelectByUserAsync();

        Task<Guid> CreateAsync(CreateGroupRequest request);

        Task UpdateAsync(UpdateGroupRequest request);

        Task DeleteAsync(Guid groupId);
    }
}
