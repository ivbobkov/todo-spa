﻿using System;

namespace TodoApp.TaskService.BusinessLogic.Group
{
    public class CreateGroupRequest
    {
        public Guid BoardId { get; set; }

        public string Name { get; set; }
    }
}
