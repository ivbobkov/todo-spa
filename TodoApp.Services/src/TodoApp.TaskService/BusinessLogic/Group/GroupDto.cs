﻿using System;

namespace TodoApp.TaskService.BusinessLogic.Group
{
    public class GroupDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public Guid BoardId { get; set; }
    }
}
