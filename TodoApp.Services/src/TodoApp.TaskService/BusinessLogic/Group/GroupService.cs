﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using AutoMapper;

using TodoApp.Common;
using TodoApp.TaskService.DataAccess;
using TodoApp.TaskService.DataAccess.Entity;

namespace TodoApp.TaskService.BusinessLogic.Group
{
    public class GroupService : IGroupService
    {
        private readonly IMapper mapper;
        private readonly ITokenProvider tokenProvider;
        private readonly IBoardRepository boardRepository;
        private readonly IGroupRepository groupRepository;

        public GroupService(
            IMapper mapper,
            ITokenProvider tokenProvider,
            IBoardRepository boardRepository,
            IGroupRepository groupRepository)
        {
            this.mapper = mapper;
            this.tokenProvider = tokenProvider;
            this.groupRepository = groupRepository;
            this.boardRepository = boardRepository;
        }

        /// <inheritdoc />
        public async Task<GroupDto> FindAsync(Guid groupId)
        {
            if (!await HasAccessToGroupAsync(groupId))
            {
                throw new AccessDeniedException();
            }

            var entity = await groupRepository.FindAsync(groupId);
            return mapper.Map<GroupDto>(entity);
        }

        /// <inheritdoc />
        public async Task<List<GroupDto>> SelectByUserAsync()
        {
            var entities = await groupRepository.SelectByUserAsync(tokenProvider.Token.UserId);
            return mapper.Map<List<GroupDto>>(entities);
        }

        /// <inheritdoc />
        public async Task<Guid> CreateAsync(CreateGroupRequest request)
        {
            if (!await HasAccessToBoardAsync(request.BoardId))
            {
                throw new AccessDeniedException();
            }

            var entity = mapper.Map<GroupEntity>(request);

            entity = await groupRepository.CreateAsync(entity);
            return entity.Id;
        }

        /// <inheritdoc />
        public async Task UpdateAsync(UpdateGroupRequest request)
        {
            if (!await HasAccessToGroupAsync(request.GroupId))
            {
                throw new AccessDeniedException();
            }

            var entity = mapper.Map<GroupEntity>(request);
            await groupRepository.UpdateAsync(entity);
        }

        /// <inheritdoc />
        public async Task DeleteAsync(Guid groupId)
        {
            if (!await HasAccessToGroupAsync(groupId))
            {
                throw new AccessDeniedException();
            }

            await groupRepository.DeleteAsync(groupId);
        }

        private async Task<bool> HasAccessToBoardAsync(Guid boardId)
        {
            return await boardRepository.HasAccessAsync(boardId, tokenProvider.Token.UserId);
        }

        private async Task<bool> HasAccessToGroupAsync(Guid groupId)
        {
            return await groupRepository.HasAccessAsync(groupId, tokenProvider.Token.UserId);
        }
    }
}
