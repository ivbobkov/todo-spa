﻿using System;

namespace TodoApp.TaskService.BusinessLogic.Card
{
    public class UpdateCardRequest
    {
        public Guid CardId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public bool Complete { get; set; }

        public Guid GroupId { get; set; }
    }
}
