﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using AutoMapper;

using TodoApp.Common;
using TodoApp.TaskService.DataAccess;
using TodoApp.TaskService.DataAccess.Entity;

namespace TodoApp.TaskService.BusinessLogic.Card
{
    public class CardService : ICardService
    {
        private readonly IMapper mapper;
        private readonly ITokenProvider tokenProvider;
        private readonly IGroupRepository groupRepository;
        private readonly ICardRepository cardRepository;

        public CardService(
            IMapper mapper,
            ITokenProvider tokenProvider,
            IGroupRepository groupRepository,
            ICardRepository cardRepository)
        {
            this.mapper = mapper;
            this.tokenProvider = tokenProvider;
            this.cardRepository = cardRepository;
            this.groupRepository = groupRepository;
        }

        /// <inheritdoc />
        public async Task<CardDto> FindAsync(Guid cardId)
        {
            if (!await HasAccessAsync(cardId))
            {
                throw new AccessDeniedException();
            }

            var entity = await cardRepository.FindAsync(cardId);
            return mapper.Map<CardDto>(entity);
        }

        /// <inheritdoc />
        public async Task<List<CardDto>> SelectByUserAsync()
        {
            var entities = await cardRepository.SelectByUserAsync(tokenProvider.Token.UserId);
            return mapper.Map<List<CardDto>>(entities);
        }

        /// <inheritdoc />
        public async Task<Guid> CreateAsync(CreateCardRequest request)
        {
            if (!await HasAccessToGroupAsync(request.GroupId))
            {
                throw new AccessDeniedException();
            }

            var entity = mapper.Map<CardEntity>(request);
            entity = await cardRepository.CreateAsync(entity);
            return entity.Id;
        }

        /// <inheritdoc />
        public async Task UpdateAsync(UpdateCardRequest request)
        {
            if (!await HasAccessAsync(request.CardId))
            {
                throw new AccessDeniedException();
            }

            var entity = mapper.Map<CardEntity>(request);
            await cardRepository.UpdateAsync(entity);
        }

        /// <inheritdoc />
        public async Task DeleteAsync(Guid cardId)
        {
            if (!await HasAccessAsync(cardId))
            {
                throw new AccessDeniedException();
            }

            await cardRepository.DeleteAsync(cardId);
        }

        private async Task<bool> HasAccessToGroupAsync(Guid groupId)
        {
            return await groupRepository.HasAccessAsync(groupId, tokenProvider.Token.UserId);
        }

        private async Task<bool> HasAccessAsync(Guid cardId)
        {
            return await cardRepository.HasAccessAsync(cardId, tokenProvider.Token.UserId);
        }
    }
}
