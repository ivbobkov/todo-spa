﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TodoApp.TaskService.BusinessLogic.Card
{
    public interface ICardService
    {
        Task<CardDto> FindAsync(Guid cardId);

        Task<List<CardDto>> SelectByUserAsync();

        Task<Guid> CreateAsync(CreateCardRequest request);

        Task UpdateAsync(UpdateCardRequest request);

        Task DeleteAsync(Guid cardId);
    }
}
