﻿using AutoMapper;

using TodoApp.TaskService.DataAccess.Entity;

namespace TodoApp.TaskService.BusinessLogic.Card
{
    public class CardServiceMapping : Profile
    {
        public CardServiceMapping()
        {
            CreateMap<CreateCardRequest, CardEntity>()
                .ForMember(x => x.Id, x => x.Ignore())
                .ForMember(x => x.Group, x => x.Ignore());

            CreateMap<UpdateCardRequest, CardEntity>()
                .ForMember(x => x.Id, x => x.MapFrom(c => c.CardId))
                .ForMember(x => x.Group, x => x.Ignore());

            CreateMap<CardEntity, CardDto>()
                .ReverseMap();
        }
    }
}
