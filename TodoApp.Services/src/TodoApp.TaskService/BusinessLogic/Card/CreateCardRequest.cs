﻿using System;

namespace TodoApp.TaskService.BusinessLogic.Card
{
    public class CreateCardRequest
    {
        public Guid GroupId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public bool Complete { get; set; }
    }
}
