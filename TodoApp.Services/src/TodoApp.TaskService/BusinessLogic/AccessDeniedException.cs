﻿using System;

namespace TodoApp.TaskService.BusinessLogic
{
    public class AccessDeniedException : Exception
    {
        public AccessDeniedException() : base("Access denied")
        {
        }
    }
}
