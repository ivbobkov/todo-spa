﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TodoApp.TaskService.BusinessLogic.Board
{
    public interface IBoardService
    {
        Task<BoardDto> FindAsync(Guid boardId);

        Task<List<BoardDto>> SelectByUserAsync();

        Task<Guid> CreateAsync(CreateBoardRequest request);

        Task UpdateAsync(UpdateBoardRequest request);

        Task DeleteAsync(Guid boardId);
    }
}
