﻿using AutoMapper;

using TodoApp.TaskService.DataAccess.Entity;

namespace TodoApp.TaskService.BusinessLogic.Board
{
    public class BoardServiceMapping : Profile
    {
        public BoardServiceMapping()
        {
            CreateMap<CreateBoardRequest, BoardEntity>()
                .ForMember(x => x.Id, x => x.Ignore())
                .ForMember(x => x.UserId, x => x.Ignore())
                .ForMember(x => x.Groups, x => x.Ignore());

            CreateMap<UpdateBoardRequest, BoardEntity>()
                .ForMember(x => x.Id, x => x.MapFrom(c => c.BoardId))
                .ForMember(x => x.UserId, x => x.Ignore())
                .ForMember(x => x.Groups, x => x.Ignore());

            CreateMap<BoardEntity, BoardDto>()
                .ReverseMap();
        }
    }
}

