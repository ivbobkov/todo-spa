﻿namespace TodoApp.TaskService.BusinessLogic.Board
{
    public class CreateBoardRequest
    {
        public string Name { get; set; }
    }
}
