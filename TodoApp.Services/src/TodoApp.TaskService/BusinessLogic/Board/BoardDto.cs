﻿using System;

namespace TodoApp.TaskService.BusinessLogic.Board
{
    public class BoardDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public Guid UserId { get; set; }
    }
}
