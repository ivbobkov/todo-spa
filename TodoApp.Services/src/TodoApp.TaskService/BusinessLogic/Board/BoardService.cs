﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using AutoMapper;

using TodoApp.Common;
using TodoApp.TaskService.DataAccess;
using TodoApp.TaskService.DataAccess.Entity;

namespace TodoApp.TaskService.BusinessLogic.Board
{
    public class BoardService : IBoardService
    {
        private readonly IMapper mapper;
        private readonly ITokenProvider tokenProvider;
        private readonly IBoardRepository boardRepository;

        public BoardService(IMapper mapper, ITokenProvider tokenProvider, IBoardRepository boardRepository)
        {
            this.mapper = mapper;
            this.tokenProvider = tokenProvider;
            this.boardRepository = boardRepository;
        }

        /// <inheritdoc />
        public async Task<BoardDto> FindAsync(Guid boardId)
        {
            if (!await HasAccessAsync(boardId))
            {
                throw new AccessDeniedException();
            }

            var entity = await boardRepository.FindAsync(boardId);
            return mapper.Map<BoardDto>(entity);
        }

        /// <inheritdoc />
        public async Task<List<BoardDto>> SelectByUserAsync()
        {
            var entities = await boardRepository.SelectByUserAsync(tokenProvider.Token.UserId);
            return mapper.Map<List<BoardDto>>(entities);
        }

        /// <inheritdoc />
        public async Task<Guid> CreateAsync(CreateBoardRequest request)
        {
            var entity = mapper.Map<BoardEntity>(request);
            entity.UserId = tokenProvider.Token.UserId;

            entity = await boardRepository.CreateAsync(entity);
            return entity.Id;
        }

        /// <inheritdoc />
        public async Task UpdateAsync(UpdateBoardRequest request)
        {
            if (!await HasAccessAsync(request.BoardId))
            {
                throw new AccessDeniedException();
            }

            var entity = mapper.Map<BoardEntity>(request);
            await boardRepository.UpdateAsync(entity);
        }

        /// <inheritdoc />
        public async Task DeleteAsync(Guid boardId)
        {
            if (!await HasAccessAsync(boardId))
            {
                throw new AccessDeniedException();
            }

            await boardRepository.DeleteAsync(boardId);
        }

        private async Task<bool> HasAccessAsync(Guid boardId)
        {
            return await boardRepository.HasAccessAsync(boardId, tokenProvider.Token.UserId);
        }
    }
}
