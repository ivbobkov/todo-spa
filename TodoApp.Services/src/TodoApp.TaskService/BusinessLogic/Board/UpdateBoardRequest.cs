﻿using System;

namespace TodoApp.TaskService.BusinessLogic.Board
{
    public class UpdateBoardRequest
    {
        public Guid BoardId { get; set; }

        public string Name { get; set; }
    }
}
