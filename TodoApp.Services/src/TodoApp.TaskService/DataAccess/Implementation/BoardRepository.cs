﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

using TodoApp.TaskService.DataAccess.Entity;

namespace TodoApp.TaskService.DataAccess.Implementation
{
    public class BoardRepository : BaseRepository, IBoardRepository
    {
        /// <inheritdoc />
        public BoardRepository(ILoggerFactory loggerFactory, TaskServiceContext context) : base(loggerFactory, context)
        {
        }

        /// <inheritdoc />
        public async Task<BoardEntity> FindAsync(Guid boardId)
        {
            return await Context.Boards.FirstOrDefaultAsync(x => x.Id == boardId);
        }

        /// <inheritdoc />
        public async Task<List<BoardEntity>> SelectByUserAsync(Guid userId)
        {
            return await Context.Boards.Where(x => x.UserId == userId).ToListAsync();
        }

        /// <inheritdoc />
        public async Task<bool> HasAccessAsync(Guid boardId, Guid userId)
        {
            return await Context.Boards.AnyAsync(p => p.Id == boardId && p.UserId == userId);
        }

        /// <inheritdoc />
        public async Task<BoardEntity> CreateAsync(BoardEntity board)
        {
            Context.Boards.Add(board);
            await Context.SaveChangesAsync();
            return board;
        }

        /// <inheritdoc />
        public async Task UpdateAsync(BoardEntity board)
        {
            var entry = await Context.Boards.FirstOrDefaultAsync(x => x.Id == board.Id);

            if (entry == null)
            {
                throw new Exception("No entity");
            }

            entry.Name = board.Name;

            Context.Boards.Update(entry);
            await Context.SaveChangesAsync();
        }

        /// <inheritdoc />
        public async Task DeleteAsync(Guid boardId)
        {
            var entry = await Context.Boards.FirstOrDefaultAsync(x => x.Id == boardId);

            if (entry == null)
            {
                throw new Exception("No entity");
            }

            Context.Boards.Remove(entry);
            await Context.SaveChangesAsync();
        }
    }
}
