﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

using TodoApp.TaskService.DataAccess.Entity;

namespace TodoApp.TaskService.DataAccess.Implementation
{
    public class CardRepository : BaseRepository, ICardRepository
    {
        /// <inheritdoc />
        public CardRepository(ILoggerFactory loggerFactory, TaskServiceContext context) : base(loggerFactory, context)
        {
        }

        /// <inheritdoc />
        public async Task<CardEntity> FindAsync(Guid cardId)
        {
            return await Context.Cards.FirstOrDefaultAsync(x => x.Id == cardId);
        }

        /// <inheritdoc />
        public async Task<bool> HasAccessAsync(Guid cardId, Guid userId)
        {
            return await Context.Cards.AnyAsync(p => p.Id == cardId && p.Group.Board.UserId == userId);
        }

        /// <inheritdoc />
        public async Task<List<CardEntity>> SelectByUserAsync(Guid userId)
        {
            return await Context.Cards.Where(x => x.Group.Board.UserId == userId).ToListAsync();
        }

        /// <inheritdoc />
        public async Task<CardEntity> CreateAsync(CardEntity card)
        {
            Context.Cards.Add(card);
            await Context.SaveChangesAsync();
            return card;
        }

        /// <inheritdoc />
        public async Task UpdateAsync(CardEntity card)
        {
            var entry = await Context.Cards.FirstOrDefaultAsync(x => x.Id == card.Id);

            if (entry == null)
            {
                throw new Exception("No entity");
            }

            entry.Name = card.Name;
            entry.Description = card.Description;
            entry.Complete = card.Complete;
            entry.GroupId = card.GroupId;

            Context.Cards.Update(entry);
            await Context.SaveChangesAsync();
        }

        /// <inheritdoc />
        public async Task DeleteAsync(Guid cardId)
        {
            var entry = await Context.Cards.FirstOrDefaultAsync(x => x.Id == cardId);

            if (entry == null)
            {
                throw new Exception("No entity");
            }

            Context.Cards.Remove(entry);
            await Context.SaveChangesAsync();
        }
    }
}
