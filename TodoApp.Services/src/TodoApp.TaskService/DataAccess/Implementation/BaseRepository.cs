﻿using System;

using Microsoft.Extensions.Logging;

namespace TodoApp.TaskService.DataAccess.Implementation
{
    public class BaseRepository : IDisposable
    {
        protected readonly ILoggerFactory LoggerFactory;
        protected readonly TaskServiceContext Context;

        public BaseRepository(ILoggerFactory loggerFactory, TaskServiceContext context)
        {
            LoggerFactory = loggerFactory;
            Context = context;
        }

        /// <inheritdoc />
        public void Dispose()
        {
            Context.Dispose();
        }
    }
}
