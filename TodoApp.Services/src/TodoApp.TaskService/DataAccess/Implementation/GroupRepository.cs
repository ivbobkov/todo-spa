﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

using TodoApp.TaskService.DataAccess.Entity;

namespace TodoApp.TaskService.DataAccess.Implementation
{
    public class GroupRepository : BaseRepository, IGroupRepository
    {
        /// <inheritdoc />
        public GroupRepository(ILoggerFactory loggerFactory, TaskServiceContext context) : base(loggerFactory, context)
        {
        }

        /// <inheritdoc />
        public async Task<GroupEntity> FindAsync(Guid groupId)
        {
            return await Context.Groups.FirstOrDefaultAsync(x => x.Id == groupId);
        }

        /// <inheritdoc />
        public async Task<bool> HasAccessAsync(Guid groupId, Guid userId)
        {
            return await Context.Groups.AnyAsync(p => p.Id == groupId && p.Board.UserId == userId);
        }

        /// <inheritdoc />
        public async Task<List<GroupEntity>> SelectByUserAsync(Guid userId)
        {
            return await Context.Groups.Where(x => x.Board.UserId == userId).ToListAsync();
        }

        /// <inheritdoc />
        public async Task<GroupEntity> CreateAsync(GroupEntity group)
        {
            Context.Groups.Add(group);
            await Context.SaveChangesAsync();
            return group;
        }

        /// <inheritdoc />
        public async Task UpdateAsync(GroupEntity group)
        {
            var entry = await Context.Groups.FirstOrDefaultAsync(x => x.Id == group.Id);

            if (entry == null)
            {
                throw new Exception("No entity");
            }

            entry.Name = group.Name;
            entry.BoardId = group.BoardId;

            Context.Groups.Update(entry);
            await Context.SaveChangesAsync();
        }

        /// <inheritdoc />
        public async Task DeleteAsync(Guid groupId)
        {
            var entry = await Context.Groups.FirstOrDefaultAsync(x => x.Id == groupId);

            if (entry == null)
            {
                throw new Exception("No entity");
            }

            Context.Groups.Remove(entry);
            await Context.SaveChangesAsync();
        }
    }
}
