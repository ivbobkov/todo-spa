﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using TodoApp.TaskService.DataAccess.Entity;

namespace TodoApp.TaskService.DataAccess
{
    public interface IGroupRepository
    {
        Task<GroupEntity> FindAsync(Guid groupId);

        Task<bool> HasAccessAsync(Guid groupId, Guid userId);

        Task<List<GroupEntity>> SelectByUserAsync(Guid userId);

        Task<GroupEntity> CreateAsync(GroupEntity group);

        Task UpdateAsync(GroupEntity group);

        Task DeleteAsync(Guid groupId);
    }
}
