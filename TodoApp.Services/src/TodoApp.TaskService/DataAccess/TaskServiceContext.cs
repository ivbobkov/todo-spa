﻿using Microsoft.EntityFrameworkCore;

using TodoApp.TaskService.DataAccess.Entity;

namespace TodoApp.TaskService.DataAccess
{
    public class TaskServiceContext : DbContext
    {
        public TaskServiceContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Configure();
        }

        public virtual DbSet<BoardEntity> Boards { get; set; }

        public virtual DbSet<GroupEntity> Groups { get; set; }
        
        public virtual DbSet<CardEntity> Cards { get; set; }
    }
}
