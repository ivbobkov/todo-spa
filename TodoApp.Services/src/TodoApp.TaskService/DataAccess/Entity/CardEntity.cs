﻿using System;

namespace TodoApp.TaskService.DataAccess.Entity
{
    public class CardEntity
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public bool Complete { get; set; }

        public Guid GroupId { get; set; }

        public virtual GroupEntity Group { get; set; }
    }
}