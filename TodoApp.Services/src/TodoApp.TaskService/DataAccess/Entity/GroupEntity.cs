﻿using System;
using System.Collections.Generic;

namespace TodoApp.TaskService.DataAccess.Entity
{
    public class GroupEntity
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public Guid BoardId { get; set; }

        public virtual BoardEntity Board { get; set; }

        public virtual ICollection<CardEntity> Cards { get; set; } = new List<CardEntity>();
    }
}