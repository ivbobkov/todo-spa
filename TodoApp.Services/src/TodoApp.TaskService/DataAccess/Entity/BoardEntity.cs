﻿using System;
using System.Collections.Generic;

namespace TodoApp.TaskService.DataAccess.Entity
{
    public class BoardEntity
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public Guid UserId { get; set; }

        public virtual ICollection<GroupEntity> Groups { get; set; } = new List<GroupEntity>();
    }
}