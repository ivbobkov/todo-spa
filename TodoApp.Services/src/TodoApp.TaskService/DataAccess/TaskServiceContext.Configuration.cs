﻿using Microsoft.EntityFrameworkCore;

using TodoApp.TaskService.DataAccess.Entity;

namespace TodoApp.TaskService.DataAccess
{
    public static class TaskServiceContextConfiguration
    {
        public static void Configure(this ModelBuilder builder)
        {
            BoardEntity(builder);
            GroupEntity(builder);
            CardEntity(builder);
        }

        private static void BoardEntity(ModelBuilder builder)
        {
            builder.Entity<BoardEntity>(config =>
            {
                config.ToTable("Board");
                config.HasMany(x => x.Groups).WithOne(x => x.Board).IsRequired();
            });
        }

        private static void GroupEntity(ModelBuilder builder)
        {
            builder.Entity<GroupEntity>(config =>
            {
                config.ToTable("Group");
                config.HasMany(x => x.Cards).WithOne(x => x.Group).IsRequired();
            });
        }

        private static void CardEntity(ModelBuilder builder)
        {
            builder.Entity<CardEntity>(config =>
            {
                config.ToTable("Card");
            });
        }
    }
}
