﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using TodoApp.TaskService.DataAccess.Entity;

namespace TodoApp.TaskService.DataAccess
{
    public interface IBoardRepository
    {
        Task<BoardEntity> FindAsync(Guid boardId);

        Task<List<BoardEntity>> SelectByUserAsync(Guid userId);

        Task<bool> HasAccessAsync(Guid boardId, Guid userId);

        Task<BoardEntity> CreateAsync(BoardEntity board);

        Task UpdateAsync(BoardEntity board);

        Task DeleteAsync(Guid boardId);
    }
}
