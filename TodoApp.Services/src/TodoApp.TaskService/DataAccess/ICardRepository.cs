﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using TodoApp.TaskService.DataAccess.Entity;

namespace TodoApp.TaskService.DataAccess
{
    public interface ICardRepository
    {
        Task<CardEntity> FindAsync(Guid cardId);

        Task<bool> HasAccessAsync(Guid cardId, Guid userId);

        Task<List<CardEntity>> SelectByUserAsync(Guid userId);

        Task<CardEntity> CreateAsync(CardEntity card);

        Task UpdateAsync(CardEntity card);

        Task DeleteAsync(Guid cardId);
    }
}
