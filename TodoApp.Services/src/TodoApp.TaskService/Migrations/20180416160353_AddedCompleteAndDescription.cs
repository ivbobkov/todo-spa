﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TodoApp.TaskService.Migrations
{
    public partial class AddedCompleteAndDescription : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Complete",
                table: "Card",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Card",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Complete",
                table: "Card");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Card");
        }
    }
}
