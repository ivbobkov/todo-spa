﻿namespace TodoApp.ApiGateway.DataAccess
{
    public interface IJwtTokenProvider
    {
        string JwtToken { get; }
    }
}
