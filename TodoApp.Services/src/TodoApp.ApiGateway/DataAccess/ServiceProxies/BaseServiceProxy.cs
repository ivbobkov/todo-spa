﻿using Microsoft.Extensions.Options;

using RestSharp;

using TodoApp.ApiGateway.Common;
using TodoApp.Common.Web;

namespace TodoApp.ApiGateway.DataAccess.ServiceProxies
{
    public abstract class BaseServiceProxy
    {
        protected RemoteEndpoint RemoteEndpoint { get; }
        protected IJwtTokenProvider JwtTokenProvider { get; }
        protected abstract string RepositoryBasePath { get; }

        protected BaseServiceProxy(IOptions<RemoteEndpoint> remoteEndpoint, IJwtTokenProvider jwtTokenProvider)
        {
            RemoteEndpoint = remoteEndpoint.Value;
            JwtTokenProvider = jwtTokenProvider;
        }

        protected TResult Execute<TResult>(string basePath, RestRequest request)
        {
            var client = new RestClient(basePath);
            var jwtToken = JwtTokenProvider.JwtToken;

            if (!string.IsNullOrWhiteSpace(jwtToken))
            {
                request.AddHeader("Authorization", $"Bearer {JwtTokenProvider.JwtToken}");
            }

            var response = client.Execute<ApiResponseDto<TResult>>(request);
            Check.Verify(response);

            return response.Data.Data;
        }

        protected TResult Execute<TResult>(RestRequest request)
        {
            return Execute<TResult>(RepositoryBasePath, request);
        }

        protected void ExecuteNoResult(string basePath, RestRequest request)
        {
            var client = new RestClient(basePath);
            var jwtToken = JwtTokenProvider.JwtToken;

            if (!string.IsNullOrWhiteSpace(jwtToken))
            {
                request.AddHeader("Authorization", $"Bearer {JwtTokenProvider.JwtToken}");
            }

            var response = client.Execute<ApiResponseDto>(request);
            Check.Verify(response);
        }

        protected void ExecuteNoResult(RestRequest request)
        {
            ExecuteNoResult(RepositoryBasePath, request);
        }
    }
}