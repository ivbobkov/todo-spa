﻿using System;
using TodoApp.ApiGateway.DataAccess.ServiceProxies.AccountService.Dto;

namespace TodoApp.ApiGateway.DataAccess.ServiceProxies.AccountService
{
    public interface IUserProxy
    {
        Guid Create(CreateUserRequest createRequest);
    }
}
