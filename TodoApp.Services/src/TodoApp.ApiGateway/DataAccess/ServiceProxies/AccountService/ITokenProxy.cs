﻿using TodoApp.ApiGateway.DataAccess.ServiceProxies.AccountService.Dto;

namespace TodoApp.ApiGateway.DataAccess.ServiceProxies.AccountService
{
    public interface ITokenProxy
    {
        string GetToken(TokenRequest request);
    }
}
