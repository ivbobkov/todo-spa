﻿using System;
using System.Collections.Generic;

using TodoApp.Common.Web;

namespace TodoApp.ApiGateway.DataAccess.ServiceProxies.AccountService.Dto
{
    public class CreateUserResponse
    {
        public CreateUserResponse(Guid userId)
        {
            UserId = userId;
        }

        public CreateUserResponse(List<ApiError> errors)
        {
            Errors = errors;
        }

        public Guid UserId { get; set; }

        public List<ApiError> Errors { get; set; } = new List<ApiError>();
    }
}
