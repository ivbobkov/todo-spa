﻿using Microsoft.Extensions.Options;

using RestSharp;

using TodoApp.ApiGateway.Common;
using TodoApp.ApiGateway.DataAccess.ServiceProxies.AccountService.Dto;

namespace TodoApp.ApiGateway.DataAccess.ServiceProxies.AccountService.Internal
{
    public class TokenProxy : BaseServiceProxy, ITokenProxy
    {
        /// <inheritdoc />
        protected override string RepositoryBasePath => RemoteEndpoint.AccountService;

        /// <inheritdoc />
        public TokenProxy(IOptions<RemoteEndpoint> remoteEndpoint, IJwtTokenProvider jwtTokenProvider) : base(remoteEndpoint, jwtTokenProvider)
        {
        }

        /// <inheritdoc />
        public string GetToken(TokenRequest request)
        {
            var restRequest = new RestRequest("/api/tokens", Method.POST);
            restRequest.AddJsonBody(request);
            return Execute<string>(restRequest);
        }
    }
}
