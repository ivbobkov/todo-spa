﻿using System;

using Microsoft.Extensions.Options;

using RestSharp;

using TodoApp.ApiGateway.Common;
using TodoApp.ApiGateway.DataAccess.ServiceProxies.AccountService.Dto;

namespace TodoApp.ApiGateway.DataAccess.ServiceProxies.AccountService.Internal
{
    public class UserProxy : BaseServiceProxy, IUserProxy
    {
        /// <inheritdoc />
        protected override string RepositoryBasePath => RemoteEndpoint.AccountService;

        /// <inheritdoc />
        public UserProxy(IOptions<RemoteEndpoint> remoteEndpoint, IJwtTokenProvider jwtTokenProvider) : base(remoteEndpoint, jwtTokenProvider)
        {
        }

        /// <inheritdoc />
        public Guid Create(CreateUserRequest createRequest)
        {
            var restRequest = new RestRequest("/api/users", Method.POST);
            restRequest.AddJsonBody(createRequest);
            return Execute<Guid>(restRequest);
        }
    }
}
