﻿using System.Net;

using RestSharp;

using TodoApp.Common.Web;

namespace TodoApp.ApiGateway.DataAccess.ServiceProxies
{
    internal static class Check
    {
        public static void Verify(IRestResponse<ApiResponseDto> response)
        {
            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new CommunicationException("Connection", "Wrong http response from remote service.");
            }

            if (response.Data.Code != ApiResponseCode.Success)
            {
                throw new CommunicationException(response.Data);
            }
        }

        public static void Verify<TPayload>(IRestResponse<ApiResponseDto<TPayload>> response)
        {
            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new CommunicationException("Connection", "Wrong http response from remote service.");
            }

            if (response.Data.Code != ApiResponseCode.Success)
            {
                throw new CommunicationException(response.Data);
            }
        }
    }
}
