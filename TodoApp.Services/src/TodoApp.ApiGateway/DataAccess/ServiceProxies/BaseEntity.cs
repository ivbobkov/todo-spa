﻿using System;

namespace TodoApp.ApiGateway.DataAccess.ServiceProxies
{
    public abstract class BaseEntity
    {
        public virtual Guid Id { get; set; }
    }
}