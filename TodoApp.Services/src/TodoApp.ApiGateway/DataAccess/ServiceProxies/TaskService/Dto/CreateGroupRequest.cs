﻿using System;

namespace TodoApp.ApiGateway.DataAccess.ServiceProxies.TaskService.Dto
{
    public class CreateGroupRequest
    {
        public CreateGroupRequest(GroupEntity entity)
        {
            BoardId = entity.BoardId;
            Name = entity.Name;
        }

        public string Name { get; set; }

        public Guid BoardId { get; set; }
    }
}
