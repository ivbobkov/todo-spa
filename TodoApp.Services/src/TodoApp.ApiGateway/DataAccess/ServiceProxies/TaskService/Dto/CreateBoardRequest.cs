﻿using System;

namespace TodoApp.ApiGateway.DataAccess.ServiceProxies.TaskService.Dto
{
    public class CreateBoardRequest
    {
        public CreateBoardRequest(BoardEntity entity)
        {
            Name = entity.Name;
            UserId = entity.UserId;
        }

        public string Name { get; set; }

        public Guid UserId { get; set; }
    }
}
