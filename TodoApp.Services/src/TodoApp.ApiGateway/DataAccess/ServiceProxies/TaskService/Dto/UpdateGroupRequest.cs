﻿using System;

namespace TodoApp.ApiGateway.DataAccess.ServiceProxies.TaskService.Dto
{
    public class UpdateGroupRequest
    {
        public UpdateGroupRequest(GroupEntity entity)
        {
            Name = entity.Name;
            BoardId = entity.BoardId;
        }

        public string Name { get; set; }

        public Guid BoardId { get; set; }
    }
}
