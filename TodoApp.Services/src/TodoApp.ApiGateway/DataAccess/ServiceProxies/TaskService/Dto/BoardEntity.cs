﻿using System;

namespace TodoApp.ApiGateway.DataAccess.ServiceProxies.TaskService.Dto
{
    public class BoardEntity : BaseEntity
    {
        public string Name { get; set; }

        public Guid UserId { get; set; }
    }
}