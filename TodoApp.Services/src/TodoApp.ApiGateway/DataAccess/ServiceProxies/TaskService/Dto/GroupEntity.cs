﻿using System;

namespace TodoApp.ApiGateway.DataAccess.ServiceProxies.TaskService.Dto
{
    public class GroupEntity : BaseEntity
    {
        public string Name { get; set; }

        public Guid BoardId { get; set; }
    }
}