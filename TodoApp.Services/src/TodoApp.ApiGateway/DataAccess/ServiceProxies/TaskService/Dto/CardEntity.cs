﻿using System;

namespace TodoApp.ApiGateway.DataAccess.ServiceProxies.TaskService.Dto
{
    public class CardEntity : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Complete { get; set; }

        public Guid GroupId { get; set; }
    }
}