﻿namespace TodoApp.ApiGateway.DataAccess.ServiceProxies.TaskService.Dto
{
    public class UpdateBoardRequest
    {
        public UpdateBoardRequest(BoardEntity entity)
        {
            Name = entity.Name;
        }

        public string Name { get; set; }
    }
}
