﻿using System;

namespace TodoApp.ApiGateway.DataAccess.ServiceProxies.TaskService.Dto
{
    public class UpdateCardRequest
    {
        public UpdateCardRequest(CardEntity entity)
        {
            Name = entity.Name;
            Description = entity.Description;
            Complete = entity.Complete;
            GroupId = entity.GroupId;
        }

        public string Name { get; set; }

        public string Description { get; set; }

        public bool Complete { get; set; }

        public Guid GroupId { get; set; }
    }
}
