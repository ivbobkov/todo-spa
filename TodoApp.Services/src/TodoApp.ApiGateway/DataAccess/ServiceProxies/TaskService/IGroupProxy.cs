﻿using System;
using System.Collections.Generic;

using TodoApp.ApiGateway.DataAccess.ServiceProxies.TaskService.Dto;

namespace TodoApp.ApiGateway.DataAccess.ServiceProxies.TaskService
{
    public interface IGroupProxy
    {
        IList<GroupEntity> SelectAll();
        GroupEntity Create(GroupEntity entity);
        GroupEntity Update(GroupEntity entity);
        void Delete(Guid groupId);
    }
}