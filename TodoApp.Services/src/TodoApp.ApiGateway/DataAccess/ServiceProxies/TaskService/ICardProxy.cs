﻿using System;
using System.Collections.Generic;

using TodoApp.ApiGateway.DataAccess.ServiceProxies.TaskService.Dto;

namespace TodoApp.ApiGateway.DataAccess.ServiceProxies.TaskService
{
    public interface ICardProxy
    {
        IList<CardEntity> SelectAll();
        CardEntity Create(CardEntity entity);
        CardEntity Update(CardEntity entity);
        void Delete(Guid cardId);
    }
}