﻿using System;
using System.Collections.Generic;

using TodoApp.ApiGateway.DataAccess.ServiceProxies.TaskService.Dto;

namespace TodoApp.ApiGateway.DataAccess.ServiceProxies.TaskService
{
    public interface IBoardProxy
    {
        IList<BoardEntity> SelectAll();
        BoardEntity Create(BoardEntity entity);
        BoardEntity Update(BoardEntity entity);
        void Delete(Guid boardId);
    }
}