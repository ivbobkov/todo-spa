﻿using System;
using System.Collections.Generic;

using Microsoft.Extensions.Options;

using RestSharp;

using TodoApp.ApiGateway.Common;
using TodoApp.ApiGateway.DataAccess.ServiceProxies.TaskService.Dto;

namespace TodoApp.ApiGateway.DataAccess.ServiceProxies.TaskService.Internal
{
    public class CardProxy : BaseServiceProxy, ICardProxy
    {
        /// <inheritdoc />
        protected override string RepositoryBasePath => RemoteEndpoint.TaskService;

        /// <inheritdoc />
        public CardProxy(IOptions<RemoteEndpoint> remoteEndpoint, IJwtTokenProvider jwtTokenProvider) : base(remoteEndpoint, jwtTokenProvider)
        {
        }

        public IList<CardEntity> SelectAll()
        {
            var request = new RestRequest("/api/cards", Method.GET);
            return Execute<List<CardEntity>>(request);
        }

        public CardEntity Create(CardEntity entity)
        {
            var request = new RestRequest("/api/cards", Method.POST);
            request.AddJsonBody(new CreateCardRequest(entity));
            entity.Id = Execute<Guid>(request);
            return entity;
        }

        public CardEntity Update(CardEntity entity)
        {
            var request = new RestRequest("/api/cards/{cardId}", Method.PUT);
            request.AddUrlSegment("cardId", entity.Id);
            request.AddJsonBody(new UpdateCardRequest(entity));
            ExecuteNoResult(request);
            return entity;
        }

        public void Delete(Guid cardId)
        {
            var request = new RestRequest("/api/cards/{cardId}", Method.DELETE);
            request.AddUrlSegment("cardId", cardId);
            ExecuteNoResult(request);
        }
    }
}