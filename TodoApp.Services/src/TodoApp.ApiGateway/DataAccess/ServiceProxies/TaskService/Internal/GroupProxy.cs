﻿using System;
using System.Collections.Generic;

using Microsoft.Extensions.Options;

using RestSharp;

using TodoApp.ApiGateway.Common;
using TodoApp.ApiGateway.DataAccess.ServiceProxies.TaskService.Dto;

namespace TodoApp.ApiGateway.DataAccess.ServiceProxies.TaskService.Internal
{
    public class GroupProxy : BaseServiceProxy, IGroupProxy
    {
        /// <inheritdoc />
        protected override string RepositoryBasePath => RemoteEndpoint.TaskService;

        /// <inheritdoc />
        public GroupProxy(IOptions<RemoteEndpoint> remoteEndpoint, IJwtTokenProvider jwtTokenProvider) : base(remoteEndpoint, jwtTokenProvider)
        {
        }

        public IList<GroupEntity> SelectAll()
        {
            var request = new RestRequest("/api/groups", Method.GET);
            return Execute<List<GroupEntity>>(request);
        }

        public GroupEntity Create(GroupEntity entity)
        {
            var request = new RestRequest("/api/groups", Method.POST);
            request.AddJsonBody(new CreateGroupRequest(entity));
            entity.Id = Execute<Guid>(request);
            return entity;
        }

        public GroupEntity Update(GroupEntity entity)
        {
            var request = new RestRequest("/api/groups/{groupId}", Method.PUT);
            request.AddUrlSegment("groupId", entity.Id);
            request.AddJsonBody(new UpdateGroupRequest(entity));
            ExecuteNoResult(request);
            return entity;
        }

        public void Delete(Guid groupId)
        {
            var request = new RestRequest("/api/groups/{groupId}", Method.DELETE);
            request.AddUrlSegment("groupId", groupId);
            ExecuteNoResult(request);
        }
    }
}