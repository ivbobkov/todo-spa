﻿using System;
using System.Collections.Generic;

using Microsoft.Extensions.Options;

using RestSharp;

using TodoApp.ApiGateway.Common;
using TodoApp.ApiGateway.DataAccess.ServiceProxies.TaskService.Dto;

namespace TodoApp.ApiGateway.DataAccess.ServiceProxies.TaskService.Internal
{
    public class BoardProxy : BaseServiceProxy, IBoardProxy
    {
        /// <inheritdoc />
        protected override string RepositoryBasePath => RemoteEndpoint.TaskService;

        /// <inheritdoc />
        public BoardProxy(IOptions<RemoteEndpoint> remoteEndpoint, IJwtTokenProvider jwtTokenProvider) : base(remoteEndpoint, jwtTokenProvider)
        {
        }

        public IList<BoardEntity> SelectAll()
        {
            var request = new RestRequest("/api/boards", Method.GET);
            return Execute<List<BoardEntity>>(request);
        }

        public BoardEntity Create(BoardEntity entity)
        {
            var request = new RestRequest("/api/boards", Method.POST);
            request.AddJsonBody(new CreateBoardRequest(entity));
            entity.Id = Execute<Guid>(request);
            return entity;
        }

        public BoardEntity Update(BoardEntity entity)
        {
            var request = new RestRequest("/api/boards/{boardId}", Method.PUT);
            request.AddUrlSegment("boardId", entity.Id);
            request.AddJsonBody(new UpdateBoardRequest(entity));
            ExecuteNoResult(request);
            return entity;
        }

        public void Delete(Guid boardId)
        {
            var request = new RestRequest("/api/boards/{boardId}", Method.DELETE);
            request.AddUrlSegment("boardId", boardId);
            ExecuteNoResult(request);
        }
    }
}