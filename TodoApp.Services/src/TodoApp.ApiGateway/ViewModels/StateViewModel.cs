﻿using System.Collections.Generic;

using TodoApp.ApiGateway.BusinessLogic.Board;
using TodoApp.ApiGateway.BusinessLogic.Card;
using TodoApp.ApiGateway.BusinessLogic.Group;

namespace TodoApp.ApiGateway.ViewModels
{
    public class StateViewModel
    {
        public IEnumerable<BoardDto> Boards { get; set; }
        public IEnumerable<GroupDto> Groups { get; set; }
        public IEnumerable<CardDto> Cards { get; set; }
    }
}