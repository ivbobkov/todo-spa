﻿namespace TodoApp.ApiGateway.ViewModels.Account
{
    public class SignInViewModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}