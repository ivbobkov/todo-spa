﻿using System;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using TodoApp.ApiGateway.Common;

namespace TodoApp.ApiGateway
{
    public class Startup
    {
        private readonly IConfigurationRoot configuration;

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables();
            configuration = builder.Build();
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.RegisterAuthentication(configuration);
            services.AddApplicationOptions(configuration);
            services.AddMvcWithValidation();
            services.AddMapping();

            return services.AddApplicationServices(configuration);
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseDeveloperExceptionPage(new DeveloperExceptionPageOptions());
            app.UseAuthentication();
            app.UseMvcWithDefaultRoute();
        }
    }
}
