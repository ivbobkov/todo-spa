﻿using System.Linq;

using Microsoft.AspNetCore.Http;

using TodoApp.ApiGateway.DataAccess;

namespace TodoApp.ApiGateway.Common
{
    public class JwtTokenProvider : IJwtTokenProvider
    {
        private readonly IHttpContextAccessor httpContextAccessor;

        public JwtTokenProvider(IHttpContextAccessor httpContextAccessor)
        {
            this.httpContextAccessor = httpContextAccessor;
        }

        /// <inheritdoc />
        public string JwtToken
        {
            get
            {
                var authHeaders = httpContextAccessor.HttpContext?.Request.Headers["Authorization"];
                var jwtToken = authHeaders.HasValue
                    ? authHeaders.Value.FirstOrDefault()?.Replace("Bearer ", string.Empty) ?? string.Empty
                    : string.Empty;

                return jwtToken;
            }
        }
    }
}
