﻿namespace TodoApp.ApiGateway.Common
{
    public class RemoteEndpoint
    {
        public string AccountService { get; set; }

        public string NotificationService { get; set; }

        public string TaskService { get; set; }
    }
}
