﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

using Newtonsoft.Json;
using TodoApp.Common;
using TodoApp.Common.Web;

namespace TodoApp.ApiGateway.Common
{
    public class ApiResult : JsonResult
    {
        public ApiResult(object value) : base(value)
        {
        }

        public ApiResult(object value, JsonSerializerSettings serializerSettings) : base(value, serializerSettings)
        {
        }

        public static IActionResult Error(ModelStateDictionary modelState)
        {
            return new ApiResult(ApiResponse.Error());
        }

        public static IActionResult Error(ApiError error)
        {
            return new ApiResult(ApiResponse.Error(error.AsList()));
        }

        public static IActionResult Success(object data)
        {
            return new ApiResult(ApiResponse.Success(data));
        }

        public static IActionResult Success()
        {
            return new ApiResult(ApiResponse.Success());
        }
    }
}