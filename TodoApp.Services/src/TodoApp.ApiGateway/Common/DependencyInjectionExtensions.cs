﻿using System;

using Autofac;
using Autofac.Extensions.DependencyInjection;

using AutoMapper;

using FluentValidation.AspNetCore;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using TodoApp.Common;

namespace TodoApp.ApiGateway.Common
{
    public static class DependencyInjectionExtensions
    {
        public static void AddMapping(this IServiceCollection services)
        {
            services.AddAutoMapper(x => x.AddProfiles(ApplicationAssemblies.ApiGateway));
        }

        public static void AddMvcWithValidation(this IServiceCollection services)
        {
            services
                .AddMvc(x => x.Filters.Add(typeof(GlobalExceptionFilter)))
                .AddFluentValidation(x => x.RegisterValidatorsFromAssembly(ApplicationAssemblies.ApiGateway));
        }

        public static void AddApplicationOptions(this IServiceCollection services, IConfigurationRoot configuration)
        {
            services.AddOptions();

            services.Configure<RemoteEndpoint>(configuration.GetSection("RemoteEndpoints"));
        }

        public static IServiceProvider AddApplicationServices(this IServiceCollection services, IConfigurationRoot configuration)
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<TokenHandler>().As<ITokenHandler>().InstancePerLifetimeScope();
            builder.Register(x =>
            {
                var secretKey = configuration.GetValue<string>("SecretKey");
                return new TokenHandlerSettings { SecretKey = secretKey };
            }).InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(ApplicationAssemblies.ApiGateway).AsImplementedInterfaces();

            builder.Populate(services);
            return new AutofacServiceProvider(builder.Build());
        }

        public static void RegisterAuthentication(this IServiceCollection services, IConfigurationRoot configuration)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(x =>
                {
                    var secretKey = configuration.GetValue<string>("SecretKey");
                    var tokenHandler = new TokenHandler(new TokenHandlerSettings { SecretKey = secretKey });
                    x.TokenValidationParameters = tokenHandler.ValidationParameters;
                });
        }
    }
}
