﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GlobalExceptionFilter.cs" company="ZigZag">
//     Copyright © ZigZag 2018
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Filters;
using TodoApp.Common;
using TodoApp.Common.Web;

namespace TodoApp.ApiGateway.Common
{
    public class GlobalExceptionFilter : IExceptionFilter
    {
        /// <inheritdoc />
        public void OnException(ExceptionContext context)
        {
            context.Result = new ApiResult(CreateErrorFromException(context.Exception));
        }

        private ApiResponseDto CreateErrorFromException(Exception exception)
        {
            if (exception is CommunicationException communicationException)
            {
                return ApiResponse.Error(communicationException.Errors);
            }

            var error = new ApiError
            {
                Key = "Error",
                Messages = new List<string>
                {
                    exception.Message
                }
            };

            return ApiResponseDto.Error(error.AsList());
        }
    }
}