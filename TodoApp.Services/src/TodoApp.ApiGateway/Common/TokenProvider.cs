﻿using System.Linq;

using Microsoft.AspNetCore.Http;

using TodoApp.Common;

namespace TodoApp.ApiGateway.Common
{
    public class TokenProvider : ITokenProvider
    {
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly ITokenHandler tokenHandler;

        public TokenProvider(IHttpContextAccessor httpContextAccessor, ITokenHandler tokenHandler)
        {
            this.httpContextAccessor = httpContextAccessor;
            this.tokenHandler = tokenHandler;
        }

        /// <inheritdoc />
        public SecurityToken Token
        {
            get
            {
                var authHeaders = httpContextAccessor.HttpContext?.Request.Headers["Authorization"];
                var jwtToken = authHeaders.HasValue
                    ? authHeaders.Value.FirstOrDefault()?.Replace("Bearer ", string.Empty) ?? string.Empty
                    : string.Empty;

                return tokenHandler.ParseToken(jwtToken);
            }
        }
    }
}
