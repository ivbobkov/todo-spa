﻿using System.Reflection;

namespace TodoApp.ApiGateway.Common
{
    public static class ApplicationAssemblies
    {
        public static Assembly ApiGateway => GetAssembly("TodoApp.ApiGateway");

        private static Assembly GetAssembly(string name)
        {
            return Assembly.Load(new AssemblyName(name));
        }
    }
}