﻿using System;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using TodoApp.ApiGateway.BusinessLogic.Card;
using TodoApp.ApiGateway.Common;

namespace TodoApp.ApiGateway.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class CardController : Controller
    {
        private readonly ICardService cardService;

        public CardController(ICardService cardService)
        {
            this.cardService = cardService;
        }

        [HttpPost("[action]")]
        public IActionResult Create([FromBody] CardDto card)
        {
            return ModelState.IsValid
                ? ApiResult.Success(cardService.Create(card))
                : ApiResult.Error(ModelState);
        }

        [HttpPut("[action]")]
        public IActionResult Update([FromBody] CardDto card)
        {
            return ModelState.IsValid
                ? ApiResult.Success(cardService.Update(card))
                : ApiResult.Error(ModelState);
        }

        [HttpDelete("[action]/{cardId}")]
        public IActionResult Delete(Guid cardId)
        {
            cardService.Delete(cardId);
            return ApiResult.Success(cardId);
        }
    }
}