﻿using System.Threading.Tasks;

using AutoMapper;

using Microsoft.AspNetCore.Mvc;

using TodoApp.ApiGateway.BusinessLogic.Account;
using TodoApp.ApiGateway.Common;
using TodoApp.ApiGateway.ViewModels.Account;

namespace TodoApp.ApiGateway.Controllers
{
    [Route("api/[controller]")]
    public class AccountController : Controller
    {
        private readonly IMapper mapper;
        private readonly IAccountService accountService;

        public AccountController(IMapper mapper, IAccountService accountService)
        {
            this.mapper = mapper;
            this.accountService = accountService;
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> SignIn([FromBody] SignInViewModel viewModel)
        {
            var request = mapper.Map<SignInRequest>(viewModel);
            var response = await accountService.SignInAsync(request);
            return ApiResult.Success(response);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> SignUp([FromBody] SignUpViewModel viewModel)
        {
            var request = mapper.Map<SignUpRequest>(viewModel);
            await accountService.SignUpAsync(request);
            return ApiResult.Success();
        }
    }
}