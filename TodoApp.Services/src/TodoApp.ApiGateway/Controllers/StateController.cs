﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using TodoApp.ApiGateway.BusinessLogic.Board;
using TodoApp.ApiGateway.BusinessLogic.Card;
using TodoApp.ApiGateway.BusinessLogic.Group;
using TodoApp.ApiGateway.Common;
using TodoApp.ApiGateway.ViewModels;

namespace TodoApp.ApiGateway.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class StateController : Controller
    {
        private readonly IBoardService boardService;
        private readonly IGroupService groupService;
        private readonly ICardService cardService;

        public StateController(
            IBoardService boardService,
            IGroupService groupService,
            ICardService cardService)
        {
            this.boardService = boardService;
            this.groupService = groupService;
            this.cardService = cardService;
        }

        [HttpGet("[action]")]
        public IActionResult Get()
        {
            return ApiResult.Success(new StateViewModel
            {
                Boards = boardService.GetAll(),
                Groups = groupService.GetAll(),
                Cards = cardService.GetAll()
            });
        }
    }
}