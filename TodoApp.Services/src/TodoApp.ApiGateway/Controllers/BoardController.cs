﻿using System;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using TodoApp.ApiGateway.BusinessLogic.Board;
using TodoApp.ApiGateway.Common;

namespace TodoApp.ApiGateway.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class BoardController : Controller
    {
        private readonly IBoardService boardService;

        public BoardController(IBoardService boardService)
        {
            this.boardService = boardService;
        }

        [HttpPost("[action]")]
        public IActionResult Create([FromBody] BoardDto board)
        {
            return ModelState.IsValid
                ? ApiResult.Success(boardService.Create(board))
                : ApiResult.Error(ModelState);
        }

        [HttpPut("[action]")]
        public IActionResult Update([FromBody] BoardDto board)
        {
            return ModelState.IsValid
                ? ApiResult.Success(boardService.Update(board))
                : ApiResult.Error(ModelState);
        }

        [HttpDelete("[action]/{boardId}")]
        public IActionResult Delete(Guid boardId)
        {
            boardService.Delete(boardId);
            return ApiResult.Success();
        }
    }
}