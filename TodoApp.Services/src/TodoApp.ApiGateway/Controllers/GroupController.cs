﻿using System;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using TodoApp.ApiGateway.BusinessLogic.Group;
using TodoApp.ApiGateway.Common;

namespace TodoApp.ApiGateway.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class GroupController : Controller
    {
        private readonly IGroupService groupService;

        public GroupController(IGroupService groupService)
        {
            this.groupService = groupService;
        }

        [HttpPost("[action]")]
        public IActionResult Create([FromBody] GroupDto group)
        {
            return ModelState.IsValid
                ? ApiResult.Success(groupService.Create(group))
                : ApiResult.Error(ModelState);
        }

        [HttpPut("[action]")]
        public IActionResult Update([FromBody] GroupDto group)
        {
            return ModelState.IsValid
                ? ApiResult.Success(groupService.Update(group))
                : ApiResult.Error(ModelState);
        }

        [HttpDelete("[action]/{groupId}")]
        public IActionResult Delete(Guid groupId)
        {
            groupService.Delete(groupId);
            return ApiResult.Success();
        }
    }
}