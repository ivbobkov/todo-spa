﻿using AutoMapper;

using TodoApp.ApiGateway.BusinessLogic.Board;
using TodoApp.ApiGateway.BusinessLogic.Card;
using TodoApp.ApiGateway.BusinessLogic.Group;
using TodoApp.ApiGateway.DataAccess.ServiceProxies.TaskService.Dto;

namespace TodoApp.ApiGateway.BusinessLogic
{
    public class DtoProfile : Profile
    {
        public DtoProfile()
        {
            CreateMap<BoardDto, BoardEntity>().ReverseMap();
            CreateMap<GroupDto, GroupEntity>().ReverseMap();
            CreateMap<CardDto, CardEntity>().ReverseMap();
        }
    }
}