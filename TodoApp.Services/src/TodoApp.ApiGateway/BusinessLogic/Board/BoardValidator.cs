using FluentValidation;

namespace TodoApp.ApiGateway.BusinessLogic.Board
{
    public class BoardValidator : AbstractValidator<BoardDto>
    {
        public BoardValidator()
        {
            RuleFor(x => x.Name).NotEmpty().Length(2, 25);
        }
    }
}