﻿using System;
using System.Collections.Generic;

namespace TodoApp.ApiGateway.BusinessLogic.Board
{
    public interface IBoardService
    {
        ICollection<BoardDto> GetAll();
        BoardDto Create(BoardDto board);
        BoardDto Update(BoardDto board);
        void Delete(Guid boardId);
    }
}