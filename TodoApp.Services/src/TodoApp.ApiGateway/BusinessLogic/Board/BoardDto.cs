﻿using System;

namespace TodoApp.ApiGateway.BusinessLogic.Board
{
    public class BoardDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}