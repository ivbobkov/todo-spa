﻿using System;
using System.Collections.Generic;

using AutoMapper;

using TodoApp.ApiGateway.DataAccess.ServiceProxies.TaskService;
using TodoApp.ApiGateway.DataAccess.ServiceProxies.TaskService.Dto;
using TodoApp.Common;

namespace TodoApp.ApiGateway.BusinessLogic.Board
{
    public class BoardService : IBoardService
    {
        private readonly IMapper mapper;
        private readonly ITokenProvider tokenProvider;
        private readonly IBoardProxy boardProxy;

        public BoardService(
            IMapper mapper,
            ITokenProvider tokenProvider,
            IBoardProxy boardProxy)
        {
            this.boardProxy = boardProxy;
            this.tokenProvider = tokenProvider;
            this.mapper = mapper;
        }

        public ICollection<BoardDto> GetAll()
        {
            var entities = boardProxy.SelectAll();
            return mapper.Map<ICollection<BoardDto>>(entities);
        }

        public BoardDto Create(BoardDto board)
        {
            var entity = mapper.Map<BoardEntity>(board);
            entity.UserId = tokenProvider.Token.UserId;
            entity = boardProxy.Create(entity);
            return mapper.Map<BoardDto>(entity);
        }

        public BoardDto Update(BoardDto board)
        {
            var entity = mapper.Map<BoardEntity>(board);
            entity = boardProxy.Update(entity);
            return mapper.Map<BoardDto>(entity);
        }

        public void Delete(Guid boardId)
        {
            boardProxy.Delete(boardId);
        }
    }
}