﻿using System;
using System.Collections.Generic;

using AutoMapper;
using TodoApp.ApiGateway.DataAccess.ServiceProxies.TaskService;
using TodoApp.ApiGateway.DataAccess.ServiceProxies.TaskService.Dto;

namespace TodoApp.ApiGateway.BusinessLogic.Card
{
    public class CardService : ICardService
    {
        private readonly IMapper mapper;
        private readonly ICardProxy cardProxy;

        public CardService(
            IMapper mapper,
            ICardProxy cardProxy)
        {
            this.mapper = mapper;
            this.cardProxy = cardProxy;
        }

        public ICollection<CardDto> GetAll()
        {
            var entities = cardProxy.SelectAll();
            return mapper.Map<ICollection<CardDto>>(entities);
        }

        public CardDto Create(CardDto card)
        {
            var entity = mapper.Map<CardEntity>(card);
            entity = cardProxy.Create(entity);
            return mapper.Map<CardDto>(entity);
        }

        public CardDto Update(CardDto card)
        {
            var entity = mapper.Map<CardEntity>(card);
            entity = cardProxy.Update(entity);
            return mapper.Map<CardDto>(entity);
        }

        public void Delete(Guid cardId)
        {
            cardProxy.Delete(cardId);
        }
    }
}