﻿using System;

namespace TodoApp.ApiGateway.BusinessLogic.Card
{
    public class CardDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Complete { get; set; }

        public Guid GroupId { get; set; }
    }
}