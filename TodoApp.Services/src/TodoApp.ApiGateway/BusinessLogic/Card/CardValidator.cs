﻿using FluentValidation;

namespace TodoApp.ApiGateway.BusinessLogic.Card
{
    public class CardValidator : AbstractValidator<CardDto>
    {
        public CardValidator()
        {
            RuleFor(x => x.Name).NotEmpty().Length(2, 25);
            RuleFor(x => x.Description).MaximumLength(100);
        }
    }
}