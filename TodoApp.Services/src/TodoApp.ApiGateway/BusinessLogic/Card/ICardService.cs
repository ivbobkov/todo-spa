﻿using System;
using System.Collections.Generic;

namespace TodoApp.ApiGateway.BusinessLogic.Card
{
    public interface ICardService
    {
        ICollection<CardDto> GetAll();
        CardDto Create(CardDto card);
        CardDto Update(CardDto card);
        void Delete(Guid cardId);
    }
}