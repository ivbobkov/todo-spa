﻿using FluentValidation;

namespace TodoApp.ApiGateway.BusinessLogic.Group
{
    public class GroupValidator : AbstractValidator<GroupDto>
    {
        public GroupValidator()
        {
            RuleFor(x => x.Name).NotEmpty().Length(2, 25);
        }
    }
}