﻿using System;
using System.Collections.Generic;

namespace TodoApp.ApiGateway.BusinessLogic.Group
{
    public interface IGroupService
    {
        ICollection<GroupDto> GetAll();
        GroupDto Create(GroupDto group);
        GroupDto Update(GroupDto group);
        void Delete(Guid groupId);
    }
}