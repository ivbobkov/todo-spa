﻿using System;
using System.Collections.Generic;

using AutoMapper;
using TodoApp.ApiGateway.DataAccess.ServiceProxies.TaskService;
using TodoApp.ApiGateway.DataAccess.ServiceProxies.TaskService.Dto;

namespace TodoApp.ApiGateway.BusinessLogic.Group
{
    public class GroupService : IGroupService
    {
        private readonly IMapper mapper;
        private readonly IGroupProxy groupProxy;

        public GroupService(
            IMapper mapper,
            IGroupProxy groupProxy)
        {
            this.mapper = mapper;
            this.groupProxy = groupProxy;
        }

        public ICollection<GroupDto> GetAll()
        {
            var entities = groupProxy.SelectAll();
            return mapper.Map<ICollection<GroupDto>>(entities);
        }

        public GroupDto Create(GroupDto group)
        {
            var entity = mapper.Map<GroupEntity>(group);
            entity = groupProxy.Create(entity);
            return mapper.Map<GroupDto>(entity);
        }

        public GroupDto Update(GroupDto group)
        {
            var entity = mapper.Map<GroupEntity>(group);
            entity = groupProxy.Update(entity);
            return mapper.Map<GroupDto>(entity);
        }

        public void Delete(Guid groupId)
        {
            groupProxy.Delete(groupId);
        }
    }
}