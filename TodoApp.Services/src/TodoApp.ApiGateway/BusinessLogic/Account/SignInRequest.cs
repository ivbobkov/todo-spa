﻿namespace TodoApp.ApiGateway.BusinessLogic.Account
{
    public class SignInRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
