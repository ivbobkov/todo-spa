﻿using System.Collections.Generic;
using TodoApp.Common.Web;

namespace TodoApp.ApiGateway.BusinessLogic.Account
{
    public class AccountResponse
    {
        public AccountResponse(string token)
        {
            Token = token;
        }

        public AccountResponse(List<ApiError> errors)
        {
            Errors = errors;
        }

        public string Token { get; set; }

        public List<ApiError> Errors { get; set; } = new List<ApiError>();
    }
}
