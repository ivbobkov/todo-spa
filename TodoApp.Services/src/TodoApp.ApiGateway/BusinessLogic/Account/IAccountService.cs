﻿using System.Threading.Tasks;

namespace TodoApp.ApiGateway.BusinessLogic.Account
{
    public interface IAccountService
    {
        Task<string> SignInAsync(SignInRequest request);

        Task SignUpAsync(SignUpRequest request);
    }
}
