﻿using System.Threading.Tasks;

using AutoMapper;
using TodoApp.ApiGateway.DataAccess.ServiceProxies.AccountService;
using TodoApp.ApiGateway.DataAccess.ServiceProxies.AccountService.Dto;

namespace TodoApp.ApiGateway.BusinessLogic.Account
{
    public class AccountService : IAccountService
    {
        private readonly IMapper mapper;
        private readonly IUserProxy userProxy;
        private readonly ITokenProxy tokenProxy;

        public AccountService(IMapper mapper, IUserProxy userProxy, ITokenProxy tokenProxy)
        {
            this.userProxy = userProxy;
            this.tokenProxy = tokenProxy;
            this.mapper = mapper;
        }

        /// <inheritdoc />
        public async Task<string> SignInAsync(SignInRequest request)
        {
            var tokenRequest = mapper.Map<TokenRequest>(request);
            var token = tokenProxy.GetToken(tokenRequest);
            return await Task.FromResult(token);
        }

        /// <inheritdoc />
        public async Task SignUpAsync(SignUpRequest request)
        {
            var userRequest = mapper.Map<CreateUserRequest>(request);
            userProxy.Create(userRequest);
            var tokenRequest = mapper.Map<SignInRequest>(request);
        }
    }
}
