cd src/TodoApp.AccountService
dotnet restore
dotnet ef database update
cd..\..

cd src/TodoApp.TaskService
dotnet restore
dotnet ef database update
cd..\..
