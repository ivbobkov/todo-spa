$secret = "2fef4044-fffe-430d-a89e-d8b8eb2f75a6"
$path = "https://www.myget.org/F/todo-app/api/v3/index.json"

dotnet build ./src/TodoApp.Common
dotnet pack ./src/TodoApp.Common
dotnet nuget push ./src/TodoApp.Common/bin/Debug/TodoApp.Common.1.0.2.nupkg -k $secret -s $path
