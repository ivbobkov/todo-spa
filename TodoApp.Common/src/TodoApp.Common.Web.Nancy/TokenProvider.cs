﻿using System.Linq;

using Nancy;

namespace TodoApp.Common.Web.Nancy
{
    public class TokenProvider : ITokenProvider
    {
        private readonly NancyContext nancyContext;

        private SecurityToken securityToken;

        public TokenProvider(NancyContext nancyContext)
        {
            this.nancyContext = nancyContext;
        }

        /// <inheritdoc />
        public SecurityToken Token
        {
            get
            {
                securityToken = securityToken == null && nancyContext?.CurrentUser != null
                    ? SecurityToken.FromClaims(nancyContext.CurrentUser.Claims.ToList())
                    : securityToken;

                return securityToken;
            }
        }
    }
}
