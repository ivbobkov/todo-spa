﻿using Nancy;

namespace TodoApp.Common.Web.Nancy
{
    public static class PipelineExtensions
    {
        public static void AddErrorHandler(this ErrorPipeline pipeline)
        {
            pipeline.AddItemToEndOfPipeline((context, exception) =>
            {
                var error = new ApiError("Response", exception.ResolveTrace());
                return ApiResponse.Error(error);
            });
        }
    }
}