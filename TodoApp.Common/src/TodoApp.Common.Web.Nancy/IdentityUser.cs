﻿using System.Security.Principal;

namespace TodoApp.Common.Web.Nancy
{
    internal class IdentityUser : IIdentity
    {
        /// <inheritdoc />
        public string AuthenticationType => "Bearer";

        /// <inheritdoc />
        public bool IsAuthenticated => true;

        /// <inheritdoc />
        public string Name { get; }

        public IdentityUser(SecurityToken token)
        {
            Name = token.Username;
        }
    }
}
