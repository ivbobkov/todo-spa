﻿using System.Security.Claims;

using Nancy;

namespace TodoApp.Common.Web.Nancy
{
    public class NancyIdentityProvider : INancyIdentityProvider
    {
        private readonly ITokenHandler tokenHandler;

        public NancyIdentityProvider(ITokenHandler tokenHandler)
        {
            this.tokenHandler = tokenHandler;
        }

        /// <inheritdoc />
        public ClaimsPrincipal GetUserIdentity(NancyContext context)
        {
            try
            {
                var header = context.Request.Headers.Authorization;
                if (string.IsNullOrWhiteSpace(header))
                {
                    return null;
                }

                var preparedHeader = header.Replace("Bearer ", string.Empty);
                var authToken = tokenHandler.ParseToken(preparedHeader);

                var user = new IdentityUser(authToken);
                var identity = new ClaimsIdentity(user, authToken.AsClaims());

                return new ClaimsPrincipal(identity.AsList());
            }
            catch
            {
                return null;
            }
        }
    }
}
