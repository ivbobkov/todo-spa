﻿using System.Security.Claims;

using Nancy;

namespace TodoApp.Common.Web.Nancy
{
    public interface INancyIdentityProvider
    {
        ClaimsPrincipal GetUserIdentity(NancyContext context);
    }
}
