﻿using System.Collections.Generic;

namespace TodoApp.Common.Web
{
    public class ApiResponseDto
    {
        public ApiResponseCode Code { get; set; }
        public List<ApiError> Errors { get; set; } = new List<ApiError>();

        public static ApiResponseDto Success()
        {
            return new ApiResponseDto
            {
                Code = ApiResponseCode.Success
            };
        }

        public static ApiResponseDto Error()
        {
            return new ApiResponseDto
            {
                Code = ApiResponseCode.Error
            };
        }

        public static ApiResponseDto Error(List<ApiError> errors)
        {
            return new ApiResponseDto
            {
                Code = ApiResponseCode.Error,
                Errors = errors
            };
        }
    }

    public class ApiResponseDto<TResult> : ApiResponseDto
    {
        public TResult Data { get; set; }

        public static ApiResponseDto<TResult> Success(TResult data)
        {
            return new ApiResponseDto<TResult>
            {
                Code = ApiResponseCode.Success,
                Data = data
            };
        }
    }
}
