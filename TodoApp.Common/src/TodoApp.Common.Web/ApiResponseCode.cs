﻿namespace TodoApp.Common.Web
{
    public enum ApiResponseCode
    {
        None = 0,
        Success = 1,
        Error = 2
    }
}
