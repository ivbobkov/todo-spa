﻿using System.Collections.Generic;

namespace TodoApp.Common.Web
{
    public class ApiError
    {
        public ApiError()
        {
        }

        public ApiError(string key, string message)
        {
            Key = key;
            Messages.Add(message);
        }

        public ApiError(string key, List<string> messages)
        {
            Key = key;
            Messages = messages;
        }

        public string Key { get; set; }

        public List<string> Messages { get; set; } = new List<string>();
    }
}