﻿using System.Collections.Generic;

namespace TodoApp.Common.Web
{
    public class CommunicationException : BaseApplicationException
    {
        public ApiResponseCode Code { get; }

        public List<ApiError> Errors { get; } = new List<ApiError>();

        public CommunicationException(ApiResponseDto response)
        {
            Code = response.Code;
            Errors = response.Errors;
        }

        public CommunicationException(string key, string value)
        {
            Errors.Add(new ApiError
            {
                Key = key,
                Messages = value.AsList()
            });
        }
    }
}