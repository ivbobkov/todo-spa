﻿using System.Collections.Generic;

namespace TodoApp.Common.Web
{
    public static class ApiResponse
    {
        public static ApiResponseDto Success()
        {
            return ApiResponseDto.Success();
        }

        public static ApiResponseDto<TResult> Success<TResult>(TResult data)
        {
            return ApiResponseDto<TResult>.Success(data);
        }

        public static ApiResponseDto Error()
        {
            return ApiResponseDto.Error();
        }

        public static ApiResponseDto Error(List<ApiError> errors)
        {
            return ApiResponseDto.Error(errors);
        }

        public static ApiResponseDto Error(ApiError error)
        {
            return ApiResponseDto.Error(error.AsList());
        }
    }
}
