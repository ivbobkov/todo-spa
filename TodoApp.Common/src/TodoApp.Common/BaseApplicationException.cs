﻿using System;

namespace TodoApp.Common
{
    public class BaseApplicationException : Exception
    {
        public BaseApplicationException()
        {
        }

        public BaseApplicationException(string message) : base(message)
        {
        }

        public BaseApplicationException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}