﻿using System;

namespace TodoApp.Common
{
    public class TokenHandlerSettings
    {
        public string SecretKey { get; set; }
        public TimeSpan ExpireIn { get;set; } = TimeSpan.FromMinutes(30);
    }
}
