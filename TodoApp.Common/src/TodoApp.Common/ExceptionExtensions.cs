﻿using System;
using System.Collections.Generic;

namespace TodoApp.Common
{
    public static class ExceptionExtensions
    {
        public static List<string> ResolveTrace(this Exception exception)
        {
            var list = new List<string>();

            var ex = exception;
            while (ex != null)
            {
                list.Add(ex.Message);
                ex = ex.InnerException;
            }

            return list;
        }
    }
}