﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TodoApp.Common
{
    public static class CollectionExtensions
    {
        public static List<T> AsList<T>(this T item)
        {
            return new List<T> { item };
        }

        public static bool HasItems<TInput>(this IEnumerable<TInput> items)
        {
            return items != null && items.Any();
        }

        public static bool HasItems<TInput>(this IEnumerable<TInput> items, Func<TInput, bool> predicate)
        {
            return items != null && items.Any(predicate);
        }
    }
}
