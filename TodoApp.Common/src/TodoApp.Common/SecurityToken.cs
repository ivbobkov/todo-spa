﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace TodoApp.Common
{
    public class SecurityToken
    {
        public Guid UserId { get; set; }

        public string Username { get; set; }

        public List<Claim> AsClaims()
        {
            var claims = new List<Claim>
            {
                new Claim(nameof(UserId), UserId.ToString("D")),
                new Claim(nameof(Username), Username),
            };

            return claims;
        }

        public static SecurityToken FromClaims(IReadOnlyCollection<Claim> claims)
        {
            var userId = claims.Single(x => x.Type == nameof(UserId)).Value;
            var username = claims.Single(x => x.Type == nameof(Username)).Value;

            return new SecurityToken
            {
                UserId = Guid.Parse(userId),
                Username = username,
            };
        }
    }
}
