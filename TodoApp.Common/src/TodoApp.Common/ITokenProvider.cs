﻿namespace TodoApp.Common
{
    public interface ITokenProvider
    {
        SecurityToken Token { get; }
    }
}
