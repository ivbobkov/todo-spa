﻿using Microsoft.IdentityModel.Tokens;

namespace TodoApp.Common
{
    public interface ITokenHandler
    {
        TokenValidationParameters ValidationParameters { get; }

        string CreateToken(SecurityToken token);

        SecurityToken ParseToken(string token);
    }
}
