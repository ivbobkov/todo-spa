﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;

using Microsoft.IdentityModel.Tokens;

namespace TodoApp.Common
{
    public class TokenHandler : ITokenHandler
    {
        private readonly JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
        private readonly TokenHandlerSettings settings;
        private readonly SymmetricSecurityKey symmetricKey;

        public TokenHandler(TokenHandlerSettings settings)
        {
            this.settings = settings;
            symmetricKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(settings.SecretKey));
        }

        /// <inheritdoc />
        public TokenValidationParameters ValidationParameters =>
            new TokenValidationParameters
            {
                ClockSkew = TimeSpan.Zero,
                IssuerSigningKey = symmetricKey,
                RequireSignedTokens = true,
                RequireExpirationTime = true,
                ValidateLifetime = true,
                ValidateAudience = false,
                ValidateIssuer = false,
            };

        /// <inheritdoc />
        public string CreateToken(SecurityToken token)
        {
            var claims = token.AsClaims();

            var nowDate = DateTime.UtcNow;
            var expireDate = nowDate.Add(settings.ExpireIn);
            var signInCredentials = new SigningCredentials(symmetricKey, SecurityAlgorithms.HmacSha256);

            var securityToken = new JwtSecurityToken(
                notBefore: nowDate,
                claims: claims,
                expires: expireDate,
                signingCredentials: signInCredentials);

            return handler.WriteToken(securityToken);
        }

        /// <inheritdoc />
        public SecurityToken ParseToken(string token)
        {
            handler.ValidateToken(token, ValidationParameters, out var securityToken);

            var returnToken = (JwtSecurityToken)securityToken;

            return SecurityToken.FromClaims(returnToken.Claims.ToList());
        }
    }
}
