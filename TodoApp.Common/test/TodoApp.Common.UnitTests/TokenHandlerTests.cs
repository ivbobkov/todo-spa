﻿using System;
using System.Threading.Tasks;

using FluentAssertions;

using Microsoft.IdentityModel.Tokens;

using Xunit;

namespace TodoApp.Common.UnitTests
{
    public class TokenHandlerTests
    {
        private readonly ITokenHandler tokenHandler;

        public TokenHandlerTests()
        {
            var settings = new TokenHandlerSettings
            {
                ExpireIn = TimeSpan.FromMinutes(30),
                SecretKey = "SUPER-SECRET-KEY"
            };

            tokenHandler = new TokenHandler(settings);
        }

        [Fact]
        public void CreateToken_ValidData()
        {
            var token = tokenHandler.CreateToken(new SecurityToken
            {
                UserId = Guid.NewGuid(),
                Username = "Test"
            });

            token.Should().NotBeEmpty();
        }

        [Fact]
        public void ParseToken_ValidToken()
        {
            var token = new SecurityToken
            {
                UserId = Guid.NewGuid(),
                Username = "Test"
            };

            // act
            var stringToken = tokenHandler.CreateToken(token);
            var securityToken = tokenHandler.ParseToken(stringToken);

            // assert
            securityToken.Should().BeEquivalentTo(token);
        }

        [Fact]
        public async Task ParseToken_ExpiredToken_ThrowsSecurityTokenExpiredException()
        {
            var handler = new TokenHandler(new TokenHandlerSettings
            {
                SecretKey = "SUPER-SECRET-KEY",
                ExpireIn = TimeSpan.FromSeconds(1)
            });

            // act / assert
            var stringToken = handler.CreateToken(new SecurityToken { Username = "Test" });
            await Task.Delay(TimeSpan.FromSeconds(2));
            Assert.Throws<SecurityTokenExpiredException>(() => handler.ParseToken(stringToken));
        }
    }
}
