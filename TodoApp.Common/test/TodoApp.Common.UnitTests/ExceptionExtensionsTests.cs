﻿using System;
using FluentAssertions;
using Xunit;

namespace TodoApp.Common.UnitTests
{
    public class ExceptionExtensionsTests
    {
        [Fact]
        public void ResolveTrace_ManyInnerExceptions_ReturnsThem()
        {
            var message3 = new Exception("The message 3");
            var message2 = new Exception("The message 2", message3);
            var message1 = new Exception("The message 1", message2);

            // act
            var trace = message1.ResolveTrace();

            // assert
            trace.Should()
                .HaveCount(3)
                .And
                .ContainInOrder(message1.Message, message2.Message, message3.Message);
        }

        [Fact]
        public void ResolveTrace_OneException_ReturnsIt()
        {
            var message = new Exception("The message");

            // act
            var trace = message.ResolveTrace();

            // assert
            trace.Should()
                .HaveCount(1)
                .And
                .Contain(message.Message);
        }
    }
}