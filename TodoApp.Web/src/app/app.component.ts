import { Component, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";

import { ApplicationState } from "./models/index";

@Component({
    selector: "app-root",
    templateUrl: "./app.component.html"
})
export class AppComponent implements OnInit {
    showLoader = false;

    constructor(private store: Store<ApplicationState>) { }

    ngOnInit(): void {
        this.store.select(x => x.fetching).subscribe(x => {
            this.showLoader = x;
        });
    }
}
