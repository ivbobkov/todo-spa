﻿import {
    ApplicationState,
    BoardModel,
    CardModel,
    GroupModel
} from "../models/index";
import { Action } from "@ngrx/store";

export enum AppActionTypes {
    InitState = "[Core] Set Application State",
    DropState = "[Core] Drop Application State",
    SetAuthentificated = "[Core] Set Authentificated",
    SetFetching = "[Core] Set Fetching",
    SetToken = "[Core] Set Token",

    CreateBoard = "[Board] Create",
    DeleteBoard = "[Board] Delete",
    UpdateBoard = "[Board] Update",

    CreateGroup = "[Group] Create",
    DeleteGroup = "[Group] Delete",
    UpdateGroup = "[Group] Update",

    CreateCard = "[Card] Create",
    DeleteCard = "[Card] Delete",
    UpdateCard = "[Card] Update",
    SetCompleteCard = "[Card] Set Complete Card"
}

export type ActionTypes =
    SetApplicationState &
    DropApplicationState &
    SetAuthenticated &
    SetFetching &
    SetToken &
    CreateBoard &
    DeleteBoard &
    UpdateBoard &
    CreateGroup &
    DeleteGroup &
    UpdateGroup &
    CreateCard &
    DeleteCard &
    UpdateCard &
    SetCompleteCard;

// Common

export class SetApplicationState implements Action {
    readonly type: string = AppActionTypes.InitState;
    constructor(public state: ApplicationState) { }
}

export class DropApplicationState implements Action {
    readonly type: string = AppActionTypes.DropState;
    constructor() { }
}

export class SetAuthenticated implements Action {
    readonly type: string = AppActionTypes.SetAuthentificated;
    constructor(public isAuthentificated: boolean) { }
}

export class SetFetching implements Action {
    readonly type: string = AppActionTypes.SetFetching;
    constructor(public isFetching: boolean) { }
}

export class SetToken implements Action {
    readonly type: string = AppActionTypes.SetToken;
    constructor(public token: string) { }
}

// Boards

export class CreateBoard implements Action {
    readonly type: string = AppActionTypes.CreateBoard;
    constructor(public boardModel: BoardModel) { }
}

export class DeleteBoard implements Action {
    readonly type: string = AppActionTypes.DeleteBoard;
    constructor(public boardId: string) { }
}

export class UpdateBoard implements Action {
    readonly type: string = AppActionTypes.UpdateBoard;
    constructor(public boardModel: BoardModel) { }
}

// Groups

export class CreateGroup implements Action {
    readonly type: string = AppActionTypes.CreateGroup;
    constructor(public groupModel: GroupModel) { }
}

export class DeleteGroup implements Action {
    readonly type: string = AppActionTypes.DeleteGroup;
    constructor(public groupId: string) { }
}

export class UpdateGroup implements Action {
    readonly type: string = AppActionTypes.UpdateGroup;
    constructor(public groupModel: GroupModel) { }
}

// Cards

export class CreateCard implements Action {
    readonly type: string = AppActionTypes.CreateCard;
    constructor(public cardModel: CardModel) { }
}

export class DeleteCard implements Action {
    readonly type: string = AppActionTypes.DeleteCard;
    constructor(public cardId: string) { }
}

export class UpdateCard implements Action {
    readonly type: string = AppActionTypes.UpdateCard;
    constructor(public cardModel: CardModel) { }
}

export class SetCompleteCard implements Action {
    readonly type: string = AppActionTypes.SetCompleteCard;
    constructor(public cardId: string, public isCompleted: boolean) { }
}
