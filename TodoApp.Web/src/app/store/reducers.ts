﻿import {
    ApplicationState,
    BoardModel,
    CardModel,
    GroupModel
} from "../models/index";

import { ActionTypes, AppActionTypes } from "../store/actions";

export function getEmptyState(): ApplicationState {
    return {
        boards: [],
        groups: [],
        cards: [],
        authenticated: false,
        initialized: false,
        fetching: false,
        authToken: null
    }
}

export function rootReducer(state: ApplicationState = getEmptyState(), action: ActionTypes): ApplicationState {
    switch (action.type) {
        case AppActionTypes.InitState:
            return Object.assign({}, state, {
                boards: action.state.boards,
                groups: action.state.groups,
                cards: action.state.cards,
                initialized: true,
                fetching: false
            });

        case AppActionTypes.DropState:
            return Object.assign({}, state, getEmptyState());

        case AppActionTypes.SetAuthentificated:
            return Object.assign({}, state, { authenticated: action.isAuthentificated });

        case AppActionTypes.SetFetching:
            return Object.assign({}, state, { fetching: action.isFetching });

        case AppActionTypes.SetToken:
        return Object.assign({}, state, { authToken: action.token })

        case AppActionTypes.SetToken:
            return Object.assign({}, state, { token: action.token });

        case AppActionTypes.CreateBoard:
        case AppActionTypes.DeleteBoard:
        case AppActionTypes.UpdateBoard:
            return Object.assign({}, state, { boards: boardReducer(state.boards, action) });

        case AppActionTypes.CreateGroup:
        case AppActionTypes.DeleteGroup:
        case AppActionTypes.UpdateGroup:
            return Object.assign({}, state, { groups: groupReducer(state.groups, action) });

        case AppActionTypes.CreateCard:
        case AppActionTypes.DeleteCard:
        case AppActionTypes.UpdateCard:
        case AppActionTypes.SetCompleteCard:
            return Object.assign({}, state, { cards: cardReducer(state.cards, action) });

        default:
            return state;
    }
}

export function boardReducer(boards: BoardModel[], action: ActionTypes): BoardModel[] {
    switch (action.type) {
        case AppActionTypes.CreateBoard:
            return [...boards, action.boardModel];

        case AppActionTypes.DeleteBoard:
            return boards.filter(x => x.id !== action.boardId);

        case AppActionTypes.UpdateBoard: {
            return boards.map(item => {
                if (item.id === action.boardModel.id) {
                    return Object.assign({}, item, {
                        name: action.boardModel.name
                    });
                }
                return item;
            });
        }

        default:
            return boards;
    }
}

export function groupReducer(groups: GroupModel[], action: ActionTypes): GroupModel[] {
    switch (action.type) {
        case AppActionTypes.CreateGroup:
            return [...groups, action.groupModel];

        case AppActionTypes.DeleteGroup:
            return groups.filter(x => x.id !== action.groupId);

        case AppActionTypes.UpdateGroup: {
            return groups.map(item => {
                if (item.id === action.groupModel.id) {
                    return Object.assign({}, item, {
                        name: action.groupModel.name,
                        boardId: action.groupModel.boardId
                    });
                }
                return item;
            });
        }

        default:
            return groups;
    }
}

export function cardReducer(cards: CardModel[], action: ActionTypes): CardModel[] {
    switch (action.type) {
        case AppActionTypes.CreateCard:
            return [...cards, action.cardModel];

        case AppActionTypes.DeleteCard:
            return cards.filter(x => x.id !== action.cardId);

        case AppActionTypes.UpdateCard: {
            return cards.map(item => {
                if (item.id === action.cardModel.id) {
                    return Object.assign({}, item, {
                        name: action.cardModel.name,
                        description: action.cardModel.description
                    });
                }
                return item;
            });
        }

        case AppActionTypes.SetCompleteCard: {
            return cards.map(item => {
                if (item.id === action.cardId) {
                    return Object.assign({}, item, {
                        complete: action.isCompleted
                    });
                }
                return item;
            });
        }

        default:
            return cards;
    }
}