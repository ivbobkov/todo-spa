﻿import { BoardModel } from "./board.model";
import { GroupModel } from "./group.model";
import { CardModel } from "./card.model";

export interface ApplicationState {
    boards: BoardModel[];
    groups: GroupModel[];
    cards: CardModel[];

    authenticated: boolean;
    initialized: boolean;
    fetching: boolean;

    authToken: string;
}

export interface State {
    layout: ApplicationState
}