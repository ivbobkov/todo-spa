﻿export class GroupModel {
    id: string;
    name: string;
    boardId: string;
}