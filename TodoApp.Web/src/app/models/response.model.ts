﻿export enum ApiResponseCode {
    None = 0,
    Success = 1,
    Error = 2
}

export class ApiResponseError {
    key: string;
    messages: string[] = [];
}

export class ApiResponse<TResult> {
    code: ApiResponseCode = ApiResponseCode.None;
    errors: ApiResponseError[] = [];
    data: TResult;
}

export class ApiHelper {
    static isValid<TResult>(response: ApiResponse<TResult>): boolean {
        return response.code === ApiResponseCode.Success && response.errors.length === 0;
    }
}