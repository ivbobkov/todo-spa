﻿export class CardModel {
    id: string;
    name: string;
    description: string;
    complete: boolean;

    groupId: string;
}