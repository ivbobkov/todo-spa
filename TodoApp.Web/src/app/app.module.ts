﻿import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";

import { StoreModule } from "@ngrx/store";
import { StoreDevtoolsModule } from "@ngrx/store-devtools";
import { AppRoutingModule } from "./app-routing.module";

import { rootReducer } from "./store/reducers";
import { AuthGuard, StateResolver } from "./guards/index";
import { AccountService, BoardService, CardService, GroupService } from "./services/index";

import { AppComponent } from "./app.component";
import {
    AccountLayoutComponent,
    ApplicationLayoutComponent,

    BoardItemComponent,
    BoardItemCreateComponent,
    BoardListComponent,

    CardCreateComponent,
    CardEditComponent,
    CardItemComponent,
    CardListComponent,
    
    GroupCreateComponent,
    GroupEditComponent,
    GroupItemComponent,
    GroupListComponent,

    SignInComponent,
    SignUpComponent,
    TopMenuComponent
} from "./components/index";

import { CommonModule } from "./common/common.module";

@NgModule({
    declarations: [
        AppComponent,
        AccountLayoutComponent,
        ApplicationLayoutComponent,

        BoardItemComponent,
        BoardItemCreateComponent,
        BoardListComponent,

        CardCreateComponent,
        CardEditComponent,
        CardItemComponent,
        CardListComponent,

        GroupCreateComponent,
        GroupEditComponent,
        GroupItemComponent,
        GroupListComponent,

        SignInComponent,
        SignUpComponent,
        TopMenuComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        StoreModule.forRoot({ layout: rootReducer }),
        StoreDevtoolsModule.instrument(),
        AppRoutingModule,

        CommonModule
    ],
    providers: [
        AccountService,
        BoardService,
        CardService,
        GroupService,
        AuthGuard,
        StateResolver
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
