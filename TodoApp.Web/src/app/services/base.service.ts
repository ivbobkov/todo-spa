import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Store } from "@ngrx/store";

import { ApplicationState, ApiResponse, State } from "../models/index";
import { SetFetching } from "../store/actions";
import { Observable } from "rxjs/Observable";

@Injectable()
export abstract class BaseService {
    constructor(
        protected http: HttpClient,
        protected router: Router,
        protected store: Store<State>) { }

    protected get state(): ApplicationState {
        let state: ApplicationState;
        this.store.subscribe(x => state = x.layout);
        return state;
    }

    protected get headers(): HttpHeaders {
        let headers = new HttpHeaders();
        let state = this.state;

        if (state) {
            headers = headers.append("Authorization", "Bearer " + this.state.authToken);
        }

        return headers;
    }

    protected httpGet<T>(url: string): Promise<ApiResponse<T>> {
        let observable = this.http.get(url, { headers: this.headers });
        return this.sendRequest(observable);
    }

    protected httpPost<T>(url: string, data: any): Promise<ApiResponse<T>> {
        let observable = this.http.post(url, data, { headers: this.headers });
        return this.sendRequest(observable);
    }

    protected httpPut<T>(url: string, data: any): Promise<ApiResponse<T>> {
        let observable = this.http.put(url, data, { headers: this.headers });
        return this.sendRequest(observable);
    }

    protected httpDelete<T>(url: string): Promise<ApiResponse<T>> {
        let observable = this.http.delete(url, { headers: this.headers });
        return this.sendRequest(observable);
    }

    private sendRequest<T>(observable: Observable<any>): Promise<ApiResponse<T>> {
        this.store.dispatch(new SetFetching(true));
        return observable
            .toPromise()
            .then((value) => {
                this.store.dispatch(new SetFetching(false));
                return Promise.resolve(value);
            })
            .catch((value) => {
                this.store.dispatch(new SetFetching(false));
                return Promise.reject(value);
            });
    }
}