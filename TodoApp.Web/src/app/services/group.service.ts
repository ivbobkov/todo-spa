﻿import { Injectable } from "@angular/core";
import { select } from "@ngrx/store";
import { Observable } from "rxjs/observable";

import { BaseService } from "./base.service";
import * as models from "../models/index"; 
import * as actions from "../store/actions";

@Injectable()
export class GroupService extends BaseService {
    groups(boardId: string): Observable<models.GroupModel[]> {
        return this.store.pipe(select(x => x.layout.groups.filter(c => c.boardId === boardId)));
    }

    group(groupId: string): Observable<models.GroupModel> {
        return this.store.pipe(select(x => x.layout.groups.find(c => c.id === groupId)));
    }

    createGroup(group: models.GroupModel) {
        this.httpPost<models.GroupModel>("/api/Group/Create", group)
            .then(x => this.store.dispatch(new actions.CreateGroup(x.data)));
    }

    deleteGroup(id: string) {
        this.httpDelete<void>(`/api/Group/Delete/${id}`)
            .then(x => this.store.dispatch(new actions.DeleteGroup(id)));
    }

    updateGroup(group: models.GroupModel) {
        this.httpPut<models.GroupModel>("/api/Group/Update", group)
            .then(x => this.store.dispatch(new actions.UpdateGroup(x.data)));
    }
}