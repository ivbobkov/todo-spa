﻿import { Injectable } from "@angular/core";

import { BaseService } from "./base.service";
import { ApiResponse, SignInModel, SignUpModel, ApiHelper } from "../models/index";

import * as actions from "../store/actions";

@Injectable()
export class AccountService extends BaseService {
    signIn(model: SignInModel): Promise<ApiResponse<string>> {
        return this.httpPost<string>("/api/account/signin", model).then(response => {
            if (ApiHelper.isValid(response)) {
                this.store.dispatch(new actions.SetToken(response.data));
                this.store.dispatch(new actions.SetAuthenticated(true));
            }
            return response;
        });
    }

    signUp(model: SignUpModel): Promise<ApiResponse<string>> {
        return this.httpPost<string>("/api/account/signup", model)
            .then(response => {
                if (ApiHelper.isValid(response)) {
                    this.store.dispatch(new actions.SetToken(response.data));
                    this.store.dispatch(new actions.SetAuthenticated(true));
                }
                return response;
            });
    }

    signOut(): Promise<any> {
        this.store.dispatch(new actions.SetToken(null));
        this.store.dispatch(new actions.SetAuthenticated(false));
        this.store.dispatch(new actions.DropApplicationState());

        return Promise.resolve();
    }
}