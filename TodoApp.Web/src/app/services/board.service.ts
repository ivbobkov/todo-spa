﻿import { Injectable } from "@angular/core";
import { select } from "@ngrx/store";
import { Observable } from "rxjs/observable";

import { BaseService } from "./base.service";
import * as models from "../models/index";
import * as actions from "../store/actions";

@Injectable()
export class BoardService extends BaseService {
    get boards(): Observable<models.BoardModel[]> {
        return this.store.pipe(select(x => x.layout.boards));
    }

    createBoard(board: models.BoardModel) {
        this.httpPost<models.BoardModel>("/api/Board/Create", board)
            .then(x => this.store.dispatch(new actions.CreateBoard(x.data)));
    }

    deleteBoard(id: string) {
        this.httpDelete<void>(`/api/Board/Delete/${id}`)
            .then(() => this.store.dispatch(new actions.DeleteBoard(id)));
    }

    updateBoard(board: models.BoardModel) {
        this.httpPut<models.BoardModel>("/api/Board/Update", board)
            .then(x => this.store.dispatch(new actions.UpdateBoard(x.data)));
    }
}