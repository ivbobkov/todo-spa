﻿import { Injectable } from "@angular/core";
import { select } from "@ngrx/store";
import { Observable } from "rxjs/observable";

import { BaseService } from "./base.service";
import * as models from "../models/index"; 
import * as actions from "../store/actions";

@Injectable()
export class CardService extends BaseService {
    cards(groupId: string): Observable<models.CardModel[]> {
        return this.store.pipe(select(x => x.layout.cards.filter(c => c.groupId === groupId)));
    }

    card(cardId: string): Observable<models.CardModel> {
        return this.store.pipe(select(x => x.layout.cards.find(c => c.id === cardId)));
    }

    createCard(card: models.CardModel) {
        this.httpPost<models.CardModel>("/api/Card/Create", card)
            .then(x => this.store.dispatch(new actions.CreateCard(x.data)));
    }

    deleteCard(id: string) {
        this.httpDelete<void>(`/api/Card/Delete/${id}`)
            .then(x => this.store.dispatch(new actions.DeleteCard(id)));
    }

    updateCard(card: models.CardModel) {
        this.httpPut<models.CardModel>("/api/Card/Update", card)
            .then(x => this.store.dispatch(new actions.UpdateCard(x.data)))
    }

    setCompleteFlag(card: models.CardModel, flag: boolean) {
        let data = Object.assign({}, card, { complete: flag });
        this.httpPut<models.CardModel>("/api/Card/Update", data)
            .then(x => this.store.dispatch(new actions.SetCompleteCard(x.data.id, flag)));
    }
}