﻿import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: "app-overlay",
    templateUrl: "./overlay.component.html",
    styleUrls: ["./overlay.component.less"]
})
export class OverlayComponent {
    @Input()
    visible = false;

    @Output()
    click = new EventEmitter<void>();

    onClick(event: MouseEvent) {
        if ((<HTMLElement>event.target).classList.contains("app-overlay")) {
            this.click.emit();
        }
    }
}