﻿import { Component, Output, EventEmitter } from '@angular/core';

@Component({
    selector: "app-modal-window",
    templateUrl: "./modal-window.component.html"
})
export class ModalWindowComponent {
    @Output()
    containerClick = new EventEmitter<any>();

    visible = false;

    show() {
        this.visible = true;
    }

    hide() {
        this.visible = false;
    }

    onContainerClicked() {
        this.containerClick.emit();
    }
}