﻿import { ViewChild } from "@angular/core";
import { FormGroup } from "@angular/forms";

import { ModalWindowComponent } from "./modal-window/modal-window.component";

export abstract class BaseFormComponent {
    @ViewChild(ModalWindowComponent)
    modal: ModalWindowComponent;

    form: FormGroup;

    getErrorClass(name: string): string {
        let control = this.form.controls[name];
        let invalid = control.invalid && control.touched;
        return invalid ? "has-error" : "";
    }

    hasLengthError(name: string): boolean {
        let control = this.form.controls[name];
        if (!control.errors) {
            return false;
        }
        let invalid = control.invalid && control.touched;
        let minLength = !!control.errors["minlength"];
        let maxLength = !!control.errors["maxlength"];
        return invalid && (minLength || maxLength);
    }

    hasRequiredError(name: string): boolean {
        let control = this.form.controls[name];
        if (!control.errors) {
            return false;
        }
        let invalid = control.invalid && control.touched;
        let required = !!control.errors["required"];
        return invalid && required;
    }

    hasEmailError(name: string): boolean {
        let control = this.form.controls[name];
        if (!control.errors) {
            return false;
        }

        let invalid = control.invalid && control.touched;

        return invalid && control.errors["email"];
    }
}