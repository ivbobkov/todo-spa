﻿import { BrowserModule } from "@angular/platform-browser";
import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import {
    LoaderComponent,
    ModalWindowComponent,
    OverlayComponent,
    PageNotFoundComponent
} from "./components/index";

@NgModule({
    declarations: [
        LoaderComponent,
        ModalWindowComponent,
        OverlayComponent,
        PageNotFoundComponent
    ],
    imports: [
        BrowserModule,
        RouterModule
    ],
    providers: [],
    exports: [
        LoaderComponent,
        ModalWindowComponent,
        OverlayComponent,
        PageNotFoundComponent
    ]
})
export class CommonModule { }
