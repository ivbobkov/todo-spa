﻿import { NgModule } from "@angular/core";
import { RouterModule, Route } from "@angular/router";

import { AuthGuard, StateResolver } from "./guards/index";
import {
    AccountLayoutComponent,
    ApplicationLayoutComponent,
    BoardListComponent,

    CardCreateComponent,
    CardEditComponent,

    GroupCreateComponent,
    GroupEditComponent,
    GroupListComponent,

    SignInComponent,
    SignUpComponent
} from "./components/index";

import { PageNotFoundComponent } from "./common/components/index";

const AppRoutes: Route[] = [
    {
        path: "",
        pathMatch: "full",
        redirectTo: "boards"
    },
    {
        path: "",
        component: ApplicationLayoutComponent,
        canActivateChild: [AuthGuard],
        resolve: [StateResolver],
        children: [
            {
                path: "boards",
                component: BoardListComponent
            }
        ]
    },
    {
        path: "board",
        component: ApplicationLayoutComponent,
        canActivateChild: [AuthGuard],
        resolve: [StateResolver],
        children: [
            {
                path: "",
                pathMatch: "full",
                redirectTo: "/boards"
            },
            {
                path: ":board-id",
                component: GroupListComponent
            },
            {
                path: ":board-id/group",
                component: GroupListComponent,
                children: [
                    {
                        path: "",
                        pathMatch: "full",
                        component: GroupCreateComponent
                    }
                ]
            },
            {
                path: ":board-id/group/:group-id",
                component: GroupListComponent,
                children: [
                    {
                        path: "",
                        pathMatch: "full",
                        component: GroupEditComponent
                    }
                ]
            },
            {
                path: ":board-id/group/:group-id/card",
                component: GroupListComponent,
                children: [
                    {
                        path: "",
                        pathMatch: "full",
                        component: CardCreateComponent
                    }
                ]
            },

            {
                path: ":board-id/group/:group-id/card/:card-id",
                component: GroupListComponent,
                children: [
                    {
                        path: "",
                        pathMatch: "full",
                        component: CardEditComponent
                    }
                ]
            },
        ]
    },
    {
        path: "account",
        component: AccountLayoutComponent,
        children: [
            {
                path: "",
                pathMatch: "full",
                redirectTo: "sign-in"
            },
            {
                path: "sign-in",
                component: SignInComponent
            },
            {
                path: "sign-up",
                component: SignUpComponent
            }
        ]
    },
    {
        path: "**",
        component: PageNotFoundComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(AppRoutes)
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }