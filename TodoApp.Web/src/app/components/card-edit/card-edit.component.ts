import { Component, ViewChild, Input, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { Location } from "@angular/common";

import { Subscription } from "rxjs";
import { BaseFormComponent } from "../../common/components/index";
import { CardService } from "../../services/index";
import { CardModel } from "../../models/card.model";

@Component({
    selector: "app-card-edit",
    templateUrl: "./card-edit.component.html"
})
export class CardEditComponent extends BaseFormComponent implements OnInit {
    private subscription: Subscription;

    form: FormGroup;
    card: CardModel;

    constructor(private location: Location, private route: ActivatedRoute, private cardService: CardService) {
        super();
        this.form = new FormGroup({
            "name": new FormControl("", [
                Validators.required,
                Validators.minLength(2),
                Validators.maxLength(25)
            ]),
            "description": new FormControl("", [
                Validators.minLength(0),
                Validators.maxLength(100)
            ])
        });
    }

    ngOnInit() {
        let cardId = this.route.snapshot.params["card-id"] as string;
        this.subscription = this.cardService.card(cardId).subscribe(card => {
            this.card = card;
            this.form.controls["name"].setValue(card.name);
            this.form.controls["description"].setValue(card.description);
            this.modal.show();
        });
    }

    onCancel() {
        this.location.back();
        this.modal.hide();
    }

    onDelete() {
        this.subscription.unsubscribe();
        this.cardService.deleteCard(this.card.id);
        this.modal.hide();
        this.location.back();
    }

    onSubmit(model: CardModel, valid: boolean) {
        if (!valid) {
            return;
        }
        let value = Object.assign({}, this.card, {
            name: model.name,
            description: model.description
        });
        this.subscription.unsubscribe();
        this.cardService.updateCard(value);
        this.modal.hide();
        this.location.back();
    }

    getErrorClass(name: string): string {
        let control = this.form.controls[name];
        let invalid = control.invalid && control.touched;
        return invalid ? "has-error" : "";
    }
}