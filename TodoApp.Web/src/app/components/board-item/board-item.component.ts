﻿import { Component, Input, Output, EventEmitter } from "@angular/core";

import { BoardModel } from "../../models/index";

@Component({
    selector: "app-board-item",
    templateUrl: "./board-item.component.html"
})
export class BoardItemComponent {
    @Input()
    board: BoardModel;

    @Input()
    link: string;

    editMode = false;

    boardName: string;

    @Output()
    delete = new EventEmitter<BoardModel>();

    @Output()
    update = new EventEmitter<BoardModel>();

    onEdit() {
        const value = !this.board.name ? "" : this.board.name;
        this.boardName = Object.assign(value, {});
        this.switchMode();
    }

    onCancel() {
        this.switchMode();
    }

    onDelete() {
        this.delete.emit(this.board);
    }

    onUpdate() {
        const copy = Object.assign(this.board, { name: this.boardName });
        this.update.emit(copy);
    }

    private switchMode() {
        this.editMode = !this.editMode;
    }
}
