import { Component } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Router } from "@angular/router";

import { select } from "@ngrx/store";

import { BaseFormComponent } from "../../common/components/index";
import { AccountService } from "../../services/account.service";
import { SignInModel } from "../../models/sign-in.model";

@Component({
    selector: "app-sign-in",
    templateUrl: "./sign-in.component.html"
})
export class SignInComponent extends BaseFormComponent {
    errors: string[] = [];

    constructor(private accountService: AccountService, private router: Router) {
        super();
        this.form = new FormGroup({
            "username": new FormControl("", [
                Validators.required,
                Validators.minLength(2),
                Validators.maxLength(25)
            ]),
            "password": new FormControl("", [
                Validators.required,
                Validators.minLength(6),
                Validators.maxLength(25)
            ])
        });
    }

    onSubmit(model: SignInModel, valid: boolean): void {
        if (!valid) {
            return;
        }
        this.accountService.signIn(model)
            .then(x => {
                if (x.errors.length) {
                    this.errors = x.errors.map(x => x.messages).reduce((a1, a2) => a1.concat(a2));
                    return;
                }
                this.router.navigate(["/boards"]);
            });
    }

    signUpLink(): string[] {
        return ["/account/sign-up"];
    }
}