﻿import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { GroupService } from "../../services/index";
import { GroupModel } from "../../models/index";

@Component({
    selector: "app-group-list",
    templateUrl: "./group-list.component.html"
})
export class GroupListComponent implements OnInit {
    private boardId: string;

    groups: GroupModel[];

    constructor(private route: ActivatedRoute, private groupService: GroupService) { }

    ngOnInit() {
        this.boardId = this.route.snapshot.params["board-id"] as string;
        this.groupService.groups(this.boardId).subscribe(groups => this.groups = groups);
    }

    getGroupCreateLink(): string {
        return `/board/${this.boardId}/group`;
    }

    getGroupEditLink(group: GroupModel): string {
        return `/board/${this.boardId}/group/${group.id}`;
    }
}