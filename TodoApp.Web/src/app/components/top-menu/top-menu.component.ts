﻿import { Component } from "@angular/core";
import { Router } from "@angular/router";

import { AccountService } from "../../services/account.service";

@Component({
    selector: "app-top-menu",
    templateUrl: "./top-menu.component.html"
})
export class TopMenuComponent {
    createGroupVisible: boolean = false;

    constructor(private accountService: AccountService, private router: Router) { }

    getBoardsLink(): string {
        return "/boards";
    }

    signOut() {
        this.accountService.signOut();
        this.router.navigate(["/account/sign-in"]);
    }
}