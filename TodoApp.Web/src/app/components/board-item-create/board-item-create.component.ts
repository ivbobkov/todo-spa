﻿import { Component, Output, EventEmitter } from "@angular/core";

@Component({
    selector: "app-board-item-create",
    templateUrl: "./board-item-create.component.html"
})
export class BoardItemCreateComponent {
    createMode = false;
    boardName: string;

    @Output()
    create = new EventEmitter<string>();

    onCreate() {
        this.boardName = "";
        this.switchMode();
    }

    onCancel() {
        this.switchMode();
    }

    onSave() {
        if (!this.boardName || this.boardName.length < 2 || this.boardName.length > 25) {
            return;
        }
        this.create.emit(this.boardName);
        this.onCreate();
    }

    private switchMode() {
        this.createMode = !this.createMode;
    }
}
