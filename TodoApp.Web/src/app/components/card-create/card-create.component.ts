import { Component, ViewChild, Input, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { Location } from "@angular/common";

import { Subscription } from "rxjs";
import { ModalWindowComponent } from "../../common/components/index";
import { CardService } from "../../services/index";
import { CardModel } from "../../models/card.model";

@Component({
    selector: "app-card-create",
    templateUrl: "./card-create.component.html"
})
export class CardCreateComponent implements OnInit {
    private groupId: string;

    @ViewChild(ModalWindowComponent)
    modal: ModalWindowComponent;

    form: FormGroup;

    constructor(private location: Location, private route: ActivatedRoute, private cardService: CardService) {
        this.form = new FormGroup({
            "name": new FormControl("", [
                Validators.required,
                Validators.minLength(2),
                Validators.maxLength(25)
            ]),
            "description": new FormControl("", [
                Validators.minLength(0),
                Validators.maxLength(100)
            ])
        });
    }

    ngOnInit() {
        this.groupId = this.route.snapshot.params["group-id"] as string;
        this.modal.show();
    }

    onCancel() {
        this.location.back();
        this.modal.hide();
    }

    onSubmit(model: CardModel, valid: boolean) {
        if (!valid) {
            return;
        }
        let card: CardModel = {
            id: undefined,
            name: model.name,
            description: model.description,
            complete: false,
            groupId: this.groupId
        };
        this.cardService.createCard(card);
        this.modal.hide();
        this.location.back();
    }

    getErrorClass(name: string): string {
        let control = this.form.controls[name];
        let invalid = control.invalid && control.touched;
        return invalid ? "has-error" : "";
    }

    hasLengthError(name: string): boolean {
        let control = this.form.controls[name];
        if (!control.errors) {
            return false;
        }
        let invalid = control.invalid && control.touched;
        let minLength = !!control.errors["minlength"];
        let maxLength = !!control.errors["maxlength"];
        return invalid && (minLength || maxLength);
    }

    hasRequiredError(name: string): boolean {
        let control = this.form.controls[name];
        if (!control.errors) {
            return false;
        }
        let invalid = control.invalid && control.touched;
        let required = !!control.errors["required"];
        return invalid && required;
    }
}