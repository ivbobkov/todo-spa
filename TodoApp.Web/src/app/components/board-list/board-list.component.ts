import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { BoardModel } from "../../models/index";
import { BoardService } from "../../services/index";

@Component({
    selector: "app-board-list",
    templateUrl: "./board-list.component.html"
})
export class BoardListComponent implements OnInit {
    boards: BoardModel[] = [];

    constructor(private boardService: BoardService, private router: Router) { }

    ngOnInit() {
        this.boardService.boards.subscribe(boards => this.boards = boards);
    }

    createBoard(boardName: string) {
        this.boardService.createBoard({ id: undefined, name: boardName });
    }

    deleteBoard(board: BoardModel) {
        this.boardService.deleteBoard(board.id);
    }

    updateBoard(board: BoardModel) {
        this.boardService.updateBoard(board);
    }

    getViewLink(board: BoardModel): string {
        return `/board/${board.id}`;
    }
}
