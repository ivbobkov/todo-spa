﻿import { Component, Input } from "@angular/core";

import { GroupModel } from "../../models/index";

@Component({
    selector: "app-group-item",
    templateUrl: "./group-item.component.html"
})
export class GroupItemComponent {
    @Input()
    group: GroupModel;

    @Input()
    link: string;
}