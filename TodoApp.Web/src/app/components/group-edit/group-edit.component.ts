import { Component, ViewChild, Input, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { Location } from "@angular/common";

import { Subscription } from "rxjs";
import { BaseFormComponent } from "../../common/components/index";
import { GroupService } from "../../services/index";
import { GroupModel } from "../../models/index";

@Component({
    selector: "app-group-edit",
    templateUrl: "./group-edit.component.html"
})
export class GroupEditComponent extends BaseFormComponent implements OnInit {
    private subscription: Subscription;

    form: FormGroup;
    group: GroupModel;

    constructor(private location: Location, private route: ActivatedRoute, private groupService: GroupService) {
        super();
        this.form = new FormGroup({
            "name": new FormControl("", [
                Validators.required,
                Validators.minLength(2),
                Validators.maxLength(25)
            ])
        });
    }

    ngOnInit() {
        this.modal.show();
        let groupId = this.route.snapshot.params["group-id"] as string;
        this.subscription = this.groupService.group(groupId).subscribe(group => {
            this.group = group;
            this.form.controls["name"].setValue(group.name);
            this.modal.show();
        });
    }

    onCancel() {
        this.location.back();
        this.modal.hide();
    }

    onDelete() {
        this.subscription.unsubscribe();
        this.groupService.deleteGroup(this.group.id);
        this.modal.hide();
        this.location.back();
    }

    onSubmit(model: GroupModel, valid: boolean) {
        if (!valid) {
            return;
        }
        let value = Object.assign({}, this.group, { name: model.name });
        this.subscription.unsubscribe();
        this.groupService.updateGroup(value);
        this.modal.hide();
        this.location.back();
    }

    getErrorClass(name: string): string {
        let control = this.form.controls[name];
        let invalid = control.invalid && control.touched;
        return invalid ? "has-error" : "";
    }
}