import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { BaseFormComponent } from "../../common/components/index";
import { SignUpModel } from "../../models/sign-up.model";
import { AccountService } from '../../services/account.service';

@Component({
    selector: "app-sign-up",
    templateUrl: "./sign-up.component.html"
})
export class SignUpComponent extends BaseFormComponent {
    errors: string[] = [];

    constructor(private accountService: AccountService, private router: Router) {
        super();
        this.form = new FormGroup({
            "email": new FormControl("", [
                Validators.required,
                Validators.email,
                Validators.minLength(2),
                Validators.maxLength(100)
            ]),
            "username": new FormControl("", [
                Validators.required,
                Validators.minLength(2),
                Validators.maxLength(25)
            ]),
            "password": new FormControl("", [
                Validators.required,
                Validators.minLength(6),
                Validators.maxLength(25)
            ]),
            "confirmPassword": new FormControl("", [
                Validators.required,
                (control) => {
                    if (!control.parent) {
                        return null;
                    }
                    let passwordInput = control.parent.get("password");
                    if (control.value === passwordInput.value) {
                        return null;
                    }
                    return {
                        "match": true
                    };
                }
            ])
        });
    }

    onSubmit(model: SignUpModel, valid: boolean): void {
        if (!valid) {
            return;
        }
        this.accountService.signUp(model)
            .then(x => {
                if (x.errors.length) {
                    this.errors = x.errors.map(x => x.messages).reduce((a1, a2) => a1.concat(a2));
                    return;
                }
                this.router.navigate(["/boards"]);
            });
    }

    signInLink(): string[] {
        return ["/account/sign-in"];
    }

    hasMatchError(name: string): boolean {
        let control = this.form.controls[name];
        if (!control.errors) {
            return false;
        }
        let invalid = control.invalid && control.touched;
        let required = !!control.errors["match"];
        return invalid && required;
    }
}