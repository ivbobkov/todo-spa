﻿import { Component, Input, Output, EventEmitter } from "@angular/core";

import { CardModel, EventBag } from "../../models/index";

@Component({
    selector: "app-card-item",
    templateUrl: "./card-item.component.html"
})
export class CardItemComponent {
    @Input()
    card: CardModel;

    @Input()
    link: string;

    @Output()
    complete = new EventEmitter<EventBag<CardModel, boolean>>();

    onComplete() {
        this.complete.emit({
            arg1: this.card,
            arg2: this.card.complete
        });
    }
}