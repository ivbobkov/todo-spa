﻿import { Component, Input, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { CardService } from "../../services/index";
import { GroupModel, CardModel, EventBag } from "../../models/index";

@Component({
    selector: "app-card-list",
    templateUrl: "./card-list.component.html"
})
export class CardListComponent implements OnInit {
    @Input()
    group: GroupModel;

    cards: CardModel[] = [];

    constructor(private router: Router, private cardService: CardService) { }

    ngOnInit(): void {
        this.cardService.cards(this.group.id).subscribe(cards => this.cards = cards);
    }

    onCreate(cardName: string) {
        this.cardService.createCard({
            id: undefined,
            name: cardName,
            description: undefined,
            complete: false,
            groupId: this.group.id
        });
    }

    onComplete(eventBag: EventBag<CardModel, boolean>) {
        this.cardService.setCompleteFlag(eventBag.arg1, !eventBag.arg2);
    }

    getCardCreateLink(): string {
        return `/board/${this.group.boardId}/group/${this.group.id}/card`;
    }

    getCardEditLink(card: CardModel) {
        return `/board/${this.group.boardId}/group/${this.group.id}/card/${card.id}`;
    }
}