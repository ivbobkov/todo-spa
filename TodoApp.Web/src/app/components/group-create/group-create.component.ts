﻿import { Component, ViewChild, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { Location } from "@angular/common";

import { BaseFormComponent } from "../../common/components/index";
import { GroupService } from "../../services/index";
import { GroupModel } from "../../models/index";

@Component({
    selector: "app-group-create",
    templateUrl: "./group-create.component.html"
})
export class GroupCreateComponent extends BaseFormComponent implements OnInit {
    private boardId: string;

    constructor(private location: Location, private route: ActivatedRoute, private groupService: GroupService) {
        super();
        this.form = new FormGroup({
            "name": new FormControl("", [
                Validators.required,
                Validators.minLength(2),
                Validators.maxLength(25)
            ])
        });
    }

    ngOnInit() {
        this.modal.show();
        this.boardId = this.route.snapshot.params["board-id"] as string;
    }

    onCancel() {
        this.location.back();
        this.modal.hide();
    }

    onSubmit(model: GroupModel, valid: boolean) {
        if (!valid) {
            return;
        }
        let group: GroupModel = { id: undefined, name: model.name, boardId: this.boardId };
        this.groupService.createGroup(group);
        this.modal.hide();
        this.location.back();
    }

    getErrorClass(name: string): string {
        let control = this.form.controls[name];
        let invalid = control.invalid && control.touched;
        return invalid ? "has-error" : "";
    }
}