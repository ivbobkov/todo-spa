﻿import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import { CanActivate, CanActivateChild, Router } from "@angular/router";

import { ApplicationState, State } from "../models/index";
import { AccountService } from "../services/account.service";

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {

    constructor(
        private accountService: AccountService,
        private router: Router,
        private store: Store<State>) { }

    canActivateChild(childRoute: any, state: any): Promise<boolean> {
        return this.isAuthenticated();
    }

    canActivate(route: any, state: any): Promise<boolean> {
        return this.isAuthenticated();
    }

    private isAuthenticated(): Promise<boolean> {
        let state = this.storeState;
        if (state && state.authenticated) {
            return Promise.resolve(true);
        }

        this.router.navigate(['/account/sign-in']);
    }

    private get storeState(): ApplicationState {
        let state: ApplicationState;
        this.store.subscribe(x => state = x.layout);
        return state;
    }
}