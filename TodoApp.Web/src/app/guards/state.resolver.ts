﻿import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Store } from "@ngrx/store";
import { Resolve } from "@angular/router";

import { ApplicationState, State, ApiResponse } from "../models/index";
import { SetApplicationState } from "../store/actions";

@Injectable()
export class StateResolver implements Resolve<ApplicationState> {
    constructor(
        private http: HttpClient,
        private store: Store<State>
    ) { }

    resolve(route: any, state: any): Promise<ApplicationState> {
        let storeState = this.storeState;
        if (!storeState.initialized && storeState.authenticated) {
            let headers = new HttpHeaders().append("Authorization", "Bearer " + this.storeState.authToken);

            return this.http.get<ApiResponse<ApplicationState>>("/api/state/get", { headers: headers })
                .toPromise()
                .then(response => {
                    this.store.dispatch(new SetApplicationState(response.data));
                    return response.data;
                });
        }
        return Promise.resolve(storeState);
    }

    private get storeState(): ApplicationState {
        let state: ApplicationState;
        this.store.subscribe(x => state = x.layout);
        return state;
    }
}